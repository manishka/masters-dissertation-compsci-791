This is repo of my data science masters dissertation.

Title: 
ABCD-SE: Automatic Bayesian Covariance Discovery - Stable Extrapolation

Author: 
Manish Ashoklal Kukreja 

Supervisors:
Dr. Yun Sing Koh and Dr. Patricia Jean Riddle

Abstract:
Machine learning algorithms are known for their predictive power - feed input to algorithm and you get a output with its likelihood or probability. 
Ensemble methods like random forest, artificial neural networks such as deep learning or statistical models, for example, support vector machines, 
often provide high accuracy. These algorithms first go through a process called "learning or modelling" the data. In learning phase, algorithms 
discover certain set of rules or parameters, usually referred as models. To most users, these learned models are seen as 
inscrutable, since many of them are not given training in machine learning or statistics.

It is a big challenge, in artificial intelligence (AI) field, to learn (or select) a model that not only predicts the data well, but also is interpretable. 
As we try to make our models more descriptive, the chances of over-fitting increases. Bayesian non-parametric modelling, specifically Gaussian processes (GP)
provide a way to fit a model that can be descriptive, interpretable and has more predictive power. GP can be learned easily on new data or model 
change-points in data via their covariance kernel function. 

This dissertation explore several key areas of an artificial intelligent system which performs data analysis through compositional kernel search 
called Automatic Bayesian Covariance Discovery (ABCD). We introduce a novel methods to stabilise the predictive performance of ABCD. From our experimental
results, we show that our methods perform better extrapolation and can also explore a much wider model space in hopes of searching a richer interpretable
structure. Thus, this dissertation is titled as Automatic Bayesian Covariance Discovery with Stable Extrapolation or ABCD-SE.
