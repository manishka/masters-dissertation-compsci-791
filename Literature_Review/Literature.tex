\chapter{Literature Review} \label{chapt:LitRev}

In this chapter, we delve into our research area of Bayesian non-parametric learning using compositional kernel search. We first explore supervised learning and its popular methods, then we discuss the need for probabilistic modelling. We further discuss limitations of parametric modelling and benefits of Bayesian non-parametric modelling. 

Furthermore, we discuss a framework for performing automated pattern discovery using compositional kernel search by comparing with several approaches developed earlier for multiple kernel learning. Finally, we conclude by discussing the research opportunities in recent methods and share the focus of this dissertation.

\section{Machine learning}

Machine learning (ML) is the branch of science that makes computers perform certain tasks like image, text or speech recognition, playing games such as `Go' or even driving cars, without being manually programmed for that task. In machine learning, the computer learns a task by analysing given data and then performs it, as a human would do. 

ML can be broadly categorized into two groups - predictive and descriptive. In predictive (also called supervised learning), the objective is to learn the relationship between the input $x$ and output $y$, when a labelled set $D$ of $N$ observations or input and output pairs is given. Sometimes, $D$ is referred as `training sample', `training data' or simply `given data'.
\begin{equation}
D = \{(x_i, y_i)\}^{N}_{i=1}
\end{equation}


In training sample, each input $x_i \in \mathbb{R}$  is a $D$-dimensional vector, that can represent simple information such as no. of passengers in a flight or weight and height of a person or complex information such as a molecular structure, image, graph and so on. They are usually referred as attributes, features or co-variates. Similarly the output $y$ can be a real number (e.g. amount of carbon emission) or a categorical variable (e.g. male or female). Furthermore, depending upon value of $y$, the learning problem can be referred to as classification (when $y$ is categorical) or regression (when $y$ is real number). 

The other broad category in machine learning is descriptive (also called unsupervised learning), in this approach, the objective is to discover the interesting patterns or knowledge in the given data. Hence, training sample contains the inputs that are not associated with any output, generally known as unlabelled inputs, 

\begin{equation}
D = \{x_i\}^{N}_{i=1}
\end{equation}

This is usually not a well-defined problem, as there is no direction given as to what pattern to look for. As a result, there are no apparent error measures that can be used, as in case of supervised learning where predicted value of $y_i$ can be compared with observed value of $y_i$ for a given $x_i$. 

There is, however, a third category in machine learning which is called reinforcement learning. This type of learning is beneficial for learning the behaviour or action when a reward or penalty signal is  received. Our research problem falls in the realm of supervised machine learning, so we continue our investigation further into that area.

\section{Supervised machine learning}
We continue to explore machine learning by discussing its most widely used form i.e. supervised learning. As mentioned earlier  depending upon the type of output target the supervised learning task can be considered as classification or regression. Following subsections discuss them in detail.

\subsection{Classification}
The objective of a classification task is to learn the mapping or relationship between inputs $x$ and outputs $y$, where $y \in \{0,1,2,..,C\}$ with $C$ being the number of classes. It is called binary classification when $C=2$ , and usually in this case $y \in \{0,1\}$. It is called  multi-class classification when $C>2$. When class labels are not mutually exclusive, then it is called multi-label classification or multiple output model. In this thesis, whenever we use the term ``classification" we are referring it to a multi-class classification with single output.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{figures/classification} 
\caption{Part (a): Represents labelled training sample of coloured shapes, along with three unlabelled test samples \cite{murphy_machine_2012}. \newline Part (b): Represents training sample as an $N\times D$  design matrix. Each $i^{th}$ row represent feature vector $x_i$ along with class label $y_i \in \{0,1\}$  \cite{murphy_machine_2012}.}
\label{fig:classification}
\end{figure}

Murphy \cite{murphy_machine_2012} suggests, a way to formalize the classification problem, by viewing it as a function approximation task. Murphy assumes, 

\begin{equation}
y=f(x)
\end{equation} 

for some unknown function $f$, and the objective of the learner is to estimate the function $f$ for a given labelled training sample. Then make predictions using,

\begin{equation}
\hat{y} = \hat{f}(x)
\end{equation}

hat symbol denotes estimate. Murphy also explains that prime objective of classifier is to make predictions on novel inputs $x_*$, i.e. the input the classifier is not trained on, it is also called test sample. Prediction on test sample is known as generalization of learner. A binary classification example is shown in Figure \ref{fig:classification}. In this example, the task is to learn which shapes belong to class label 1 and which belong to class label 0. And make prediction about the class or classify three unlabelled shapes they are blue crescent, yellow circular ring and blue arrow. 

A fair guess the learner could do is to classify blue crescent as $y=1$ , as all blue shapes appear in class 1. The yellow circular ring and blue arrow are somewhat difficult to classify as some circular rings (blue rings) are assigned $y=1$ and other $y=0$ (red rings) also, yellow shapes appear on both sides as well. 

Similar situation is faced for blue arrow classification all blue coloured shapes are assigned $y=1$ whereas a red arrow is assigned $y=0$. This requires little more than just assigning the class to unseen data or need for some way to show confidence about the generalization beyond training set.



\subsection{Regression}
This section explores regression. In general regression is just like classification except the output target $y$ is a continuous or real number. Murphy \cite{murphy_machine_2012} mentions that linear regression is similar to line fitting between $x_i,$ and $y_i$ or formally as,

\begin{equation}
y_i = \sum\limits^{D}_{d=0}{(\beta_{d} \times x_{i,d})} \quad i = \{1..N\}
\end{equation}

here, $N$ is number of observations in training data, $D$ is dimensions or features of input $x$, $\beta_0$ is offset of the line with $\{x_{i,0}= 1\}_{i=1}^N$ and $\{\beta_1, \beta_2 .. \beta_D\}$ are the coefficients $x_d$ or slope of line. Figure \ref{fig:regression} shows a simple example. In this example a one-dimensional real-valued input $x_i \in \mathbb{R}$, and response or output variable also real-valued i.e. $y \in \mathbb{R}$. Two models are fitted to the data in \ref{fig:regression} part (a) a straight line and in \ref{fig:regression} part (b) a quadratic function. 

Murphy argues that several problems can arise in this learning approach such as outliers in observations, non-linear or non-smooth responses, or high-dimensional inputs. There have been several methods developed to tackle such problems such as measuring Cook's distance \cite{mendenhall_second_2011} to detect outliers, use of quadratic function or generalized linear models for non-linear or non-smooth responses, feature selection to deal with high-dimensional inputs.

In both methods --- classification and regression, there is a need to provide some sort of confidence about the prediction that is been done by these models.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{figures/regression} 
\caption{Part (a): Represents linear regression on a one-dimensional data \cite{murphy_machine_2012}. \newline Part (b): Represents polynomial regression (degree 2) on same data \cite{murphy_machine_2012}.}
\label{fig:regression}
\end{figure}

\subsection{Predicting output as probability}
In order to handle ambiguity, such as yellow ring case in classification or use of linear versus quadratic in regression, it is best-suited to return a probability rather than a class or a real number for output $y$.  \citea{rasmussen_gaussian_2006} describe the process of generalization, on test inputs,  involves inherent uncertainty and it is natural to make predictions that depicts this intuition about uncertainties.    The probability distribution of output $y$, given  test input $x_*$ and training data $D$ is represented as $p(y|x_*,D)$.  The probabilistic output $\hat{y}$  is computed by maximizing $\hat{f}(x)$  or loss function as shown below, 
\begin{equation} 
\newcommand{\argmax}{\operatornamewithlimits{argmax}} \hat{y} = \hat{f}(x)= \argmax\limits_{c=0}^{C}  p(y=c|x_*,D)
\end{equation}
corresponds to predicting most probable class in a probabilistic classification model, often referred as mode of the probability distribution $p(y|x_*,D)$. It is known as MAP (maximum a posteriori) estimate, whereas, in regression finding the value of $\hat{\beta}$  that maximizing the log-likelihood of the $\hat{f}(x)$  is known as MLE (maximum likelihood estimator) and is given by, 
\begin{equation} 
\newcommand{\argmax}{\operatornamewithlimits{argmax}} \hat{\beta}_{mle} \subseteq \argmax\limits_{\beta\in \Theta}   \hat{l} (\beta|x_1,...,x_n)
\end{equation} 
\begin{equation}
\hat{l}(\beta|x) = \frac{1}{n} \sum\limits_{i=1}^{n} \ln f(x_i|\beta)
\end{equation}
Murphy \cite{murphy_machine_2012} describes certain real world applications where probabilistic predictions are used such as IBM's Watson system to provide an answer when asked a question based on its confidence in the answer and Google's SmartASS (ad selection system) which based on user's search history and other features predicts the probability of user clicking an ad, this probability is better known as the CTR (click through rate). The list is growing enormously due to its benefits. To continue our exploration, we discuss further about two kinds of probabilistic models.

\section{Probabilistic modelling}
There are several approaches to define probabilistic models of the form $p(y|x)$, the most important distinction among them is the whether a model has fixed number of parameters or do they grow with respect to the amount of the training set. 

\subsection{Parametric versus non-parametric models}
The model with fixed number of parameters in it, is called a parametric model. The model in which number of parameters increase as the amount of training set, is called a non-parametric model. Both these models have their own advantages and limitations over each other. 

Parametric models are often faster to build, however, they make very strong assumptions about the probability distributions of the training set. On the other hand, non-parametric models are very flexible about the nature of data distributions, on the down side they are often computationally demanding for big training sets. 

\citea{orbanz_bayesian_2011} provide some examples of each type, like fitting a Gaussian (a normal distribution) or a mixture of a fixed number of Gaussians by maximum likelihood is one way of doing parametric modelling. Parzen window estimator is given as example for non-parametric modelling, this estimator fits a Gaussian for each observation (i.e. it uses one mean parameter for every observation). Another example for non-parametric modelling is Support Vector Machine (SVM) with a Gaussian kernel, because the model complexity (i.e. number of parameter in Gaussian kernel) increases with more observations. 

The performance of non-parametric methods has been impressive over the years and therefore they gained lot of popularity in non-Bayesian (classical) statistics. On the contrary, \citea{orbanz_bayesian_2011} argues that, the theoretical results of these models are harder to prove than that of parametric models. Consequently, Bayesian non-parametric methods emerged. Their results could be proved theoretically, as they provide a Bayesian framework for model selection and adaptation using non-parametric models. Next section discusses Bayesian non-parametric models with popular examples.

\subsection{Bayesian non-parametric modelling} \label{LitRev:NonParametricModelling}
\citea{orbanz_bayesian_2011} describes Bayesian non-parametric models as being non-trivial cases. A Bayesian model defines a prior and infers posterior distributions on one-dimensional parameter space. In a non-parametric approach, however, the dimensions of parameter space should adapt with the increase in training set. \citea{wilson_covariance_2014} describes this change in parameter space as model's information capacity that grows with data. Wilson mentions that model scaling is a desirable property, because the model must explain (or express) more information as it has been trained on more data.

Thus, a Bayesian non-parametric model is the one that, contains a Bayesian model on an infinite-dimensional (or finite but unbounded dimensional) parameter space and can be analysed on a finite training data in such a way that it only uses a finite group of the available parameters to explain the training data. 

Additionally, the parameter space is usually represented by random functions and measures or probability distributions on infinite-dimensional random objects. These functions or distributions are called as stochastic processes. Dirichlet processes, Gaussian processes and beta processes are some of the examples of such stochastic processes. In practice, the Bayesian non-parametric models are named after the stochastic processes they are comprised of.

If we are to represent a full function with a Bayesian non-parametric model,  then we might need an infinite number of parameters. Chapter \ref{chapt:Background} shows that learning and inference of Bayesian non-parametric models is possible with finite computing resources. \citea{wilson_covariance_2014} provides several reasons why Bayesian non-parametric modelling is best suited for pattern discovery and extrapolation. First being the information capacity of such models, that scales with size of training dataset. Secondly, these models reflect that we do not know the exact form of the function that created the training dataset. Third reason talks about the prior information that these models can express, which allows them to make impressive generalizations. And fourth reason emphasizes on the expressiveness of such models i.e. models become more expressive when amount of available data is more.

In spite of so many advantages, Bayesian non-parametric models, and specifically, Gaussian processes have mainly been used for smoothing and interpolation on smaller datasets \cite{wilson_covariance_2014}. As these models can express rich structure of large scale datasets, they posses the potential for developing intelligent systems, that can perform modelling automatically and generalize better on future datasets. We will now look at the literature that shows how to exploit this property of rich expressiveness of Bayesian non-parametric models using Gaussian Processes.

\section{Automatic pattern discovery}

The emergence of Big Data and High Performance Computing has motivated `machine learning' and development of data-driven model as opposed to explicit and fully structured models. Several techniques have been developed, and perfected over the past two decades. These tools can automatically learn interesting mappings. Few notable examples include tools like Hidden Markov Models, Neural Networks, Bayesian Network Inference, Support Vector Machines and Decision Trees \cite{jebara_action-reaction_1998}. 

Neural networks were introduced in 1980's and have come a long way. \citea{krizhevsky_imagenet_2012} highlighted the advancement in neural network techniques for large datasets like deep neural networks. The deep neural architecture provided remarkable results on non-linear regression problems. \citea{wilson_gpatt:_2013} described the reason behind the popularity of neural networks is their ability to automatically discover the patterns in data and represent these patterns through non-linear functions. \citea{wilson_gpatt:_2013} argues that this ability to learn patterns automatically (or expressiveness) comes at the cost of interpretability. 

On the other hand, Gaussian processes research in machine learning, grew out of neural networks research \cite{neal_bayesian_1996}  and mid 1990's marked the beginning  of kernel or Bayesian non-parametric modelling in machine learning. Kernel methods such as Gaussian processes are flexible and more interpretable than neural networks, for e.g.  the prior distributions over functions can be controlled in a covariance kernel (or kernel function) via properties such as smoothness, periodicity and so on.  

In fact, \citea{neal_bayesian_1996} shows, as the number of hidden units in a Bayesian neural network approach infinity,  it becomes a Bayesian non-parametric Gaussian processes with \textit{a neural network covariance kernel}. Thus, Gaussian processes are viewed as non-parametric kernel machines that can fit any data, by automatically calibrating their complexity, these models can be interpreted by their covariance kernel and provide a probabilistic framework for learning kernel hyper-parameters.

Gaussian processes are often used as flexible statistical tools, where a human-expert manually discovers structure in data and then embeds (or hard codes) that structure into a kernel. More specifically, Gaussian processes are used with squared exponential or Mat\'ern covariance kernels. Even though these kernels represent overall structure, that structure, however, is often the simplified version of the data. 

If we put it in other way, using default kernels like Gaussian (or squared exponential) or Mat\'ern the model provides the global structure of the dataset and fail to provide local structure (or has simplified the local structure). It is not that Gaussian processes are incapable of representing local information, but they are limited by choice of kernels and their combinations \cite{wilson_covariance_2014}. 

As most of the kernel design/ selection is performed by human experts and they could only look at handful of combinations. The resultant Gaussian processes have to face this problem. There has been a significant amount of research in the area of multiple kernel learning, next section is exploring that aspect.

\subsection{Multiple Kernel Learning} \label{mkl}
Over the years researchers have come up with several methods to perform multiple kernel learning (MKL). In these methods researchers combine several kernel functions together so that rich expressiveness along with flexibility is achieved. \citea{gonen_multiple_2011} provided a taxonomy of multiple kernel learning algorithms. 

\citea{gonen_multiple_2011} divided MKL algorithms based on six key properties like learning methods, functional form, training methods, target function, base learner and computational complexity. We will look at some of those properties that are relevant to our dissertation. 

\subsubsection{Learning methods}
These categories determine which learning methods MKL algorithms use to combine the kernel functions. Categories are given as below:
\begin{enumerate}
\item \textit{Fixed rules}: In this category the kernel functions are combined without any parameters (e.g. summation or product of kernels).
\item \textit{Heuristic approaches}: The kernels are combined using a parametrized function. The parameters for this combination function are found by comparing some heuristic measure obtained separately from each kernel.
\item \textit{Optimization approaches}: In this approach the kernel combination function is parametrized as well. The parameters for this combination function are found by solving an optimization problem.
\item \textit{Bayesian approaches}: In this method, kernel combination parameters are interpreted as random variables. They are learnt by putting a prior distribution over these parameters and performing Bayesian inference.
\item \textit{Boosting approaches}: This is done by adding a new kernel in the combination until the model performance stops improving. This technique is inspired from ensemble and boosting methods.
\end{enumerate}

\subsubsection{Functional forms}
These groups represent how the kernels are combined. They have been grouped under three categories:
\begin{enumerate}
\item \textit{Linear combination}: This method of combining kernel is the most popular one. It has two sub-categories: non-weighted sum (or mean) of kernels as combined kernel and weighted sum.
\item \textit{Non-linear combination}: These methods combine kernels with non-linear functions, for example, multiplication, power, and exponentiation.
\item \textit{Data-dependent combination}: These methods assign specific kernel weights for each observation in training dataset. In this way, they try to identify local distributions in the data and learn appropriate combination of kernels for each region of data.
\end{enumerate}

\subsubsection{Target functions}
In order to select the parameters of kernel combination function, different MKL algorithms try to optimize the target function differently. They were grouped into three basic categories:
\begin{enumerate}
\item \textit{Similarity-based functions}: A kernel combination function that maximizes the similarity metric, its parameters are chosen as the best. The similarity metric is calculated  between the combined kernel matrix and an optimum kernel matrix calculated from the training data. Similarity measures can be calculated using Kullback-Leibler (KL) divergence, kernel alignment, Euclidean distance or any other similarity measure.
\item \textit{Structural risk functions}: In this framework, the MKL algorithms try to minimize the complexity of model (or sum of regularization term) and system performance (represented by an error term). Usually, structural risk functions use the $l_1$-norm, the $l_2$-norm, or a mixed-norm on the kernel weights, that are integrated in regularization term, to pick the model parameters.
\item \textit{Bayesian functions}: A kernel function is created from candidate kernels using a Bayesian formulation and its quality is measured. Generally, likelihood or the posterior is used as the target function. Model parameters that give the maximum likelihood estimate or the maximum a posteriori estimate are selected. \end{enumerate}

\subsubsection{Training methods}
MKL algorithms are grouped under two categories based on their training methods for determining the parameters of kernel combination function and parameters\footnote{Sometimes parameters of combined-kernel are referred as \textit{hyper-parameters}} of combined-kernel base learner: 

\begin{enumerate} 

\item \textit{One-step methods}: In this group, both the parameters of kernel combination function and the parameters of the base learner (or kernel-based learner) are computed in a single pass. These parameters can be learnt sequentially or simultaneously. When learning parameters sequentially, the combination function parameters are calculated first, and then using the combined kernel, a base learner is trained.  Whereas, in simultaneous approach, both sets of parameters are learned together.

\item \textit{Two-step methods}: It is an iterative approach consisting of two steps to compute both sets of parameters. Each iteration starts with updating the parameters of kernel combination function, by keeping the base learner parameters fixed. In second step, the parameters of base learner are updated, and the combination function parameters are kept fixed. These two steps are repeated until convergence.

\end{enumerate}

\subsubsection{Base Learner}
Owing to their empirical success support vector machines (SVM) and support vector regression (SVR) are the most commonly used base learners. SVM and SVR can be easily applied in two-step methods and also can be easily used in optimization problems in one-step training methods using simultaneous approach. 

Other popular base-learners, used in MKL algorithms, include kernel ridge regression (KRR), Kernel Fisher discriminant analysis (KFDA) and regularized kernel discriminant
analysis (RKDA).

The most common base learner used in Bayesian approaches is Gaussian process (GP). There are new
inference algorithms proposed for modifying the probabilistic models in order to learn both sets of parameters.



\subsection{Compositional kernel search}

Typically, multiple kernel learning involves hand crafting combinations of Gaussian kernels for specialized
applications \cite{gonen_multiple_2011} \cite{duvenaud_structure_2013}. For example, modelling low dimensional structure in high dimensional data. These learning methods provide rich expressiveness but are not intended for automatic pattern discovery and extrapolation \cite{wilson_covariance_2014}.

As described earlier Gaussian processes research progressed in machine learning from neural network research. However, Gaussian processes have a rich history in statistics, they are often used as distributions over functions, with properties controlled by a kernel \cite{rasmussen_gaussian_2006}. In spite of providing many benefits like flexibility, interpretability, consistency, simple exact learning and inference procedures, suitability for kernel learning, and impressive empirical performances \cite{rasmussen_gaussian_2006}, Gaussian processes as kernel machines are still not in widespread use. 

This is mostly due to the fact, choosing the structural form of the kernel in non-parametric regression requires considerable expertise and trail and error \cite{duvenaud_structure_2013} \cite{wilson_covariance_2014}. As a result, fields that currently rely heavily on  expertise of statisticians, machine learning researchers and data scientists are compelled to apply Gaussian processes as smoothers and interpolation on their datasets \cite{lloyd_automatic_2014}.

In order to make Gaussian processes more applicable, \citea{duvenaud_structure_2013} proposed a method (explained in following sub-sections) that could automatically combine (or compose) kernels of a Gaussian processes using simple rules such as addition and multiplication. By looking at the problem of kernel learning as that of a structure discovery, the choices of kernel can be automated. Thus, transforming the opaque art of kernel engineering to transparent science of automated kernel construction.

\subsubsection{Representing structure through kernels}
Gaussian process (GP) models use a kernel to define the covariance or similarity between any two function (or output) values:
$Cov(y, y') = k(x, x')$. The kernel represents the possible structures under the GP prior. This structure in turn determines the generalization properties of the model. By using simple rules such as summation and multiplication, kernel families can be composed to express variety of prior over functions.

Commonly used kernels families include the squared exponential ($SE$), periodic ($PER$), linear ($LIN$), and rational quadratic ($RQ$) (see Figure \ref{fig:basekernels} and Chapter \ref{chapt:Background}). The kernels which define a valid covariance function are known as positive semi-definite kernels.  These positive semi-definite kernels when added or multiplied together results in a combined kernel, that is also a semi-definite kernel. 

By exploiting this property, richly structured and interpretable kernels can be created from well understood base kernels. All of the base kernels used in this method are one-dimensional; for multidimensional inputs, kernels are constructed by adding and multiplying kernels over single dimensions. These individual dimensions are represented using sub-scripts, e.g. $SE_2$ represents an $SE$ kernel over the second dimension of $x$. Next, we look at the operators for representing the structure. Some examples of common patterns of supervised learning can be seen as sum and products of base kernels in Table \ref{tab:exampleexpr}.



\subsubsection{Summation Rule}
Superposition of individual functions is modelled by summing those kernels. It is quite possible that each of those individual kernels are representing a different structure in data. For instance functions $f_1, f_2$ are draw from independent GP priors, $f_1 \sim GP(1, k_1), f_2 \sim GP(2, k_2)$. Then summation of $f_1 \& f_2$ is given by $f := f_1 + f_2  \sim GP(1 + 2, k_1 + k_2)$.
Sums of kernels can express superposition of different GPs, possibly operating at different scales, for a time-series model. Sometimes, summing kernels gives additive structure over different dimensions in a multi-dimensional input space, this is similar to generalized additive models (GAM). These two types of structures are illustrated in rows 2 and 4 of Figure \ref{fig:structurekernels}, respectively.

\begin{table}[h]
\centering
\caption{Example expressions: Many common patterns of supervised learning\\ can be captured using sums and products of one-dimensional base kernels}
\label{tab:exampleexpr}
\begin{tabular}{l|l}
Bayesian linear regression & $LIN$ \\
Bayesian polynomial regression & $LIN \times LIN \times \ldots$\\
Generalized Fourier decomposition & $PER + PER + \ldots$ \\
Generalized additive models & $\sum\limits_{d=1}^D {SE}_d$ \\
Automatic relevance determination & $\prod\limits_{d=1}^D {SE}_d$ \\
Linear trend with local deviations & $LIN + SE$ \\
Linearly growing amplitude & $LIN \times SE$
\end{tabular}
\end{table}


\begin{figure}[h]
\centering
\includegraphics{figures/basekernels} 
\caption{Left and third columns: base kernels $k(\cdot, 0)$. Second and fourth columns: draws from a GP with each respective kernel. The x-axis has the same range on all plots \cite{duvenaud_structure_2013}.}
\label{fig:basekernels}
\end{figure}

\subsubsection{Multiplication Rule}
To account for interactions between different input dimensions, two kernels are multiplied. For instance, the multiplicative kernel $SE_1 \times  SE_3$ represents a smoothly varying function of dimensions 1 and 3 which is not constrained to be additive, in a multidimensional data. In univariate data, multiplying a kernel by $SE$ gives a way of converting global structure to local structure. For example, $PER$ corresponds to globally periodic structure, whereas $PER \times  SE$ corresponds to locally periodic structure, as shown in row 1 of Figure \ref{fig:structurekernels}.\\

\begin{figure}[h]
\centering
\includegraphics[height=.6\textheight]{figures/structurekernels} 
\caption{Examples of structures expressible by composite
kernels. Left column and third columns: composite kernels
k(; 0). Plots have same meaning as in Figure \ref{fig:basekernels} \cite{duvenaud_structure_2013}.}
\label{fig:structurekernels}
\end{figure}

Many architectures for learning complex functions, such as backpropogation convolutional networks \citea{lecun_backpropagation_1989} and sum-product networks \citea{poon_sum-product_2011}, include units which compute AND-like and OR-like operations. A composite kernel consists of sums can be viewed as an OR-like operation, since two points are considered similar if either kernel has a high value in covariance matrix. And composite kernels consists of products can be viewed as an AND-like operation: two points are considered similar only if both kernels have high values in covariance matrix. 

\subsubsection{Searching over structures} \label{litrev:search}
A wide variety of kernel structures can be created compositionally by adding and multiplying a small number of base kernels. In particular, \citea{duvenaud_structure_2013} considers the four base kernels: $SE, PER, LIN$ and $RQ$. 

Their search procedure begins by applying all base kernels to all input dimensions. Then following search operators are applied over the set of expressions:
\begin{enumerate}
\item Any sub-expression $S$ can be replaced with $S +B$, where $B$ is any base kernel family.\label{rule:1}
\item Any sub-expression $S$ can be replaced with $S \times B$, where $B$ is any base kernel family.\label{rule:2}
\item Any base kernel $B$ may be replaced with any other base kernel family $B'$.\label{rule:3}
\end{enumerate}

These operators generate all possible algebraic expressions. When these operators are applied only on base kernels with $+$ and $\times$ rules, a context-free grammar (CFG) is obtained, which generates the set of algebraic expressions. 

This algorithm uses a greedy search approach to search over this space i.e. at each stage, the highest scoring kernel is chosen and it is expanded by applying all possible operators.

These search operators are inspired from the strategies human experts use to construct composite kernels. 
For example,
\begin{itemize}
\item One can look for structure, e.g. periodicity, in the residuals of a model, and then extend the model to capture that structure. This corresponds to applying rule \ref{rule:1}.
\item One can start with structure, e.g. linearity, which is assumed to hold globally, but find that it only holds locally. This corresponds to applying rule \ref{rule:2} to obtain the structure shown in rows 1 and 3 of Figure \ref{fig:structurekernels}.
\item One can add features incrementally, analogous to algorithms like boosting, back-fitting, or forward selection. This corresponds to applying rules \ref{rule:1} or \ref{rule:2} to dimensions not yet included in the model.\\
\end{itemize}
\textbf{Scoring kernel families}\\
Choosing kernel structures requires a criterion for evaluating structures. We choose marginal likelihood as our criterion, since it balances the fit and complexity of a model \cite{rasmussen_occams_2001}. Conditioned on kernel parameters, the marginal likelihood of a GP can be computed analytically. However, to evaluate a kernel family we must integrate over kernel parameters. We approximate this intractable integral with the Bayesian information criterion (BIC) \cite{schwarz_estimating_1978} after first optimizing to find the maximum-likelihood kernel parameters.

Unfortunately, optimizing over parameters is not a convex optimization problem, and the space can have many local optima. For example, in data with periodic structure, integer multiples of the true period (i.e. harmonics) are often local optima. To alleviate this difficulty, we take advantage of our search procedure to provide reasonable initializations: all of the parameters which were part of the previous kernel are initialized to their previous values. All parameters are then optimized using conjugate gradients, randomly restarting the newly introduced parameters. This procedure is not guaranteed to find the global optimum, but it implements the commonly used heuristic of iteratively modelling residuals.

\section{Discussion of current research} \label{LitRev:Discussion}
As noted by \citea{plate_accuracy_1999}, one of the main advantages of additive models such as GAM is their interpretability.
Plate also notes that by allowing high-order interactions as well as low-order interactions,
one can trade off interpretability with predictive accuracy. By exploring structure space with the help of context-free grammar and simple search operators that combine kernels in sums and products form. Then evaluating those kernels using BIC, provides an automated framework to construct kernels that are interpretable, flexible and have good predictive accuracy \cite{duvenaud_structure_2013}. Since search operations and rules are applied to the similarity functions (or covariance kernels) rather than the regression functions (output functions) themselves, compositions of even a few base kernels are able to capture complex relationships in data which do not have a simple parametric form \cite{duvenaud_structure_2013}.

\citea{duvenaud_structure_2013} work was extended in \citea{lloyd_automatic_2014}. This extension included several changes that resulted in a system called Automated Bayesian Covariance Discovery (ABCD). ABCD added new base kernels like White noise $(WN)$, constant $(CONST)$ and removed rational quadratic $(RQ)$. New rules were added like changepoint $CP(\cdot,\cdot)$ and changewindow $CW(\cdot,\cdot)$. To prove the interpretability of discovered models a natural language processing (NLP) component was added to ABCD system. NLP components translated GP model into human readable report in English language with figures decomposing individual kernel in composite kernel.

In our view, ABCD system uses a combination of several approaches within each of the key properties, presented by \citea{gonen_multiple_2011} for multiple kernel learning algorithms. In particular, \begin{itemize} 
\item \textbf{Learning methods} for kernel combination function falls into fixed rules, Bayesian and boosting approaches. 
\item \textbf{Functional form} represents linear (non-weighted sums) and non-linear (products) combination.  
\item \textbf{Target functions} are optimized using Bayesian inference or likelihood and combined kernel function is evaluated using BIC that is similar to structural risk functions.
\item \textbf{Training methods} used in ABCD system is one-step sequential method.
\item \textbf{Base learner} is Gaussian processes.
\end{itemize}

As expressed by \citea{duvenaud_structure_2013} that their compositional kernel search does not guarantee to find global optimum for hyper-parameters. During experimentation with un-penalized marginal likelihood, overfitting was observed in ABCD system \cite{lloyd_representation_2015}. These scenarios provide us opportunity to explore other methods of hyper-parameter learning and model evaluation criteria. As a result, this dissertation focuses on the need for cross-validated hyper-parameter learning, top-k search and model evaluation using Akaike information criterion (AIC) \cite{akaike_information_1998}.


\section{Summary} \label{LitRev:Summary}
Machine learning provides exciting opportunities to develop data-driven intelligent systems. The dependency on human-expert is significantly influencing the choice of models created by such intelligent systems. 

There is a need for an automated framework that can discover patterns from given dataset and learn basic components that are interpretable and overall model is flexible enough to generalize on unseen data. To fully realise the dream of automated predictive systems, designing such a framework is important. 

Machine learning community is trying to solve this problem by proposing several methods. ABCD is one such system that attempts to bridge the gap between accuracy and interpretability with flexible and automated modelling. 