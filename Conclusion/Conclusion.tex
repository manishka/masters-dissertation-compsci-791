\chapter{Conclusion} \label{chapt:Conclusion}

In this section, we relate our results back to the contributions that were stated in the beginning of this dissertation. We examine the evidence obtained from the results of our experiments to show how each of our contributions are supported. We also draw out attention to any of our aims that were not supported by our experiments and limitations within our techniques. We discuss future work required to further validate the claims about providing stable extrapolation with high interpretability, made by CV SoD evaluation method.

Finally, we reflect on the work we have done, considering the context of its subject area. We explore why and how our work is an important contribution to a rapidly developing, important and incredibly useful field.



\section{Achievements} \label{Conclusion:Achievements}
We laid out three objectives in the beginning of this dissertation, they were tied together to achieve our main goal of improving prediction performance of an automatic data-analysis system known as Automatic Bayesian Covariance Discovery (ABCD) system.

The following list highlights the major achievements of this work:
\begin{itemize}
	\item \textbf{Wider search strategy}: We implemented a much wider search strategy called top-k method. Models searched through top-k $(k=3)$ strategy explored $\approx200\%$ more model space than greedy approach. This strategy has potential to exploit high performance parallel processing that modern hardware platforms provide.  
	\item \textbf{Model selection based on information loss}: We studied the effect of scoring or penalising the models using Akaike information criterion (AIC) by estabilishing the philosphy that each model represents certain information about the data and the model that has lowest information loss (or low AIC score) was selected as best.
	\item \textbf{Novel model approximation technique}: We developed CV SoD method --- a new approximation technique to evaluate Gaussian process regression models. This technique combined the simplicity of ``subset of data'' (SoD) method with stability of ``cross-validation'' (CV). Experiments with four base kernels strongly suggest that CV SoD method is indeed better at extrapolation than Hybrid SoD method.
\end{itemize}

\section{Limitations} \label{Conclusion:Limitations}
In this section, we discuss the limitations in our proposed methods and the objectives which we could not achieve through this work.

Our proposed approximation method of CV-SoD showed a little stabilisation over Hybrid SoD with models containing $\kWN$ kernel or where there isn't much variability in given data. However, most of the real world data has high variability and if the aim is to provide better extrapolation, we really should not be including White Noise ($\kWN$) in the base kernels. As including $\kWN$ kernel may model ``noise in data'' as signal.

Our proposed method of using top-k search, often times result in selection of models that are very similar to each other but they all have high scores. This could prevent the algorithm from finding richer more diverse structures. It is very similar to problem of \textit{convergence} in genetic algorithms.

On the other hand, we thought AIC would provide better predictive accuracy as it penalises (or scores) models by taking into the account of size of training data. However, based on our experimentation and result analysis we conclude that in order to improve the model selection we should be looking at ways to reduce or approximate kernel parameters rather that just depending upon information criterion.

\section{Future Directions} \label{Conclusion:FutureDirections}
This section provides future research opportunities using ABCD system to further improve and enhance its abilities as a data science AI. We discuss these into three areas - model search, model evaluation and model selection. We draw inferences from our developed methods, experimentation and their findings.

\subsection*{Model search}
Our proposed method of searching model space in top-k fashion often results in models that are very similar to each other in their structure. This does not allow the system to discover newer and richer structures and could not perform better than greedy search. Just exploring wider space is not enough, we need to keep the models in top-k pool that are also diverse by means of an entropy or novelty measure. It would be interesting to evaluate distinct top-k search methods or random walk approaches.

Furthermore, experimentation with four base kernels excluding noise kernel on current implementation of top-k method is required to provide statistical evidence to reject the hypothesis that greedy search discovers best composition.

\subsection*{Model evaluation}
Our proposed method of combining careful selection of subset of data for evaluating Gaussian process model needs further testing to validate the claim that CV SoD provides better and more accurate predictions. It would be interesting to assess the improvement in prediction accuracy by performing model evaluation through other approximation techniques like Fully Independent Training Conditional (FITC), Improved Fast Gauss Transform (IFGT) or other Markov chain Monte Carlo (MCMC) methods.

\subsection*{Model selection}
As discussed in Chapter \ref{chapt:ExpDes}, model selection is one of the most explored research areas of machine learning. Further work could be done in this area using ABCD system by proposing a better multimodel inference technique that is based on entropy that weighs dissimilarity more.

\section{Reflections} \label{Conclusion:Reflections}
As our reliance on digital systems increase for our routine and mundane tasks, the amount of data these systems, and our interactions with them, generate is reaching new heights every day. To discover meaningful patterns from large sets of data we require systems that can analyse and explain the data autonomously. ABCD is one such system that is trying to make sense out of the data, in a meaningful way. With cheap high computing power available at our dispense, we should be designing our data analysis systems in such a way, that they can explore large hypotheses space in parallel to make decisions like model selection.

Through the course of this work, we feel confident that such automatic data analysis systems can become better at learning and predicting the given data, by means of probabilistic non-parametric modelling. We therefore, think that our contributions would be deemed important as they are based on a deep understanding of how ABCD and Gaussian processes work.
