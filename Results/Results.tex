\chapter{Findings of experiments} \label{chapt:Results}
In this chapter, we present the results of experiments described in Section \ref{sec:expdesc}, then we analyse those results and present our findings. We have structured this chapter as per the experiments described in Section \ref{sec:expdesc}. Each section in this chapter is divided into two subsection, the first subsection shows the results and following subsection discusses the results.

\section{Baseline experiments}
ABCD system runs parallel jobs to search for best covariance kernel composition. Gaussian process regression is performed using GPML toolbox. \citea{lloyd_representation_2015} used Matlab to interface with GPML toolbox. We decided to use GNU Octave for our experiments, due to reason that Matlab (statistics package) licenses on NeSI were shared. The license sharing resulted in long waiting times and ABCD system became inefficient in running parallel model search jobs. 

For that reason, we performed these baseline experiments to assess the impact of using GNU Octave software for interfacing with GPML toolbox inside ABCD system. 

\subsection{Results}
We performed baseline experiments on four univariate datasets that were used by \citea{lloyd_representation_2015}. We also kept same parameter settings as in original study. We ran these experiments once. Table \ref{tab:bexpoct} shows the negative log likelihood (NLL), BIC score and number of kernel hyper-parameters of the best model discovered after ten levels of search. Table \ref{tab:bexpmat} shows the result from original study  \cite{lloyd_representation_2015}. These results are visualised in Figure \ref{fig:bexpbic}.

\begin{table}[h]
\centering
\caption{Baseline experiments results showing the best model using GNU Octave}
\label{tab:bexpoct}
\bgroup
\def\arraystretch{1.5}
\begin{tabular}{|l|c|c|c|}
\hline
\multicolumn{1}{|c|}{\textbf{Dataset}} & \textbf{NLL} & \textbf{BIC} & \textbf{\# parameters} \\ \hline
01-airline & 536.4491 & 1117.6264 & 9 \\ \hline
06-internet & 9427.7907 & 18993.7365 & 20 \\ \hline
07-call-centre & 772.1245 & 1611.7574 & 13 \\ \hline
08-radio & 226.6810 & 519.1297 & 12 \\ \hline
\end{tabular}
\egroup
\end{table}


\begin{table}[h]
\centering
\caption{Results showing best model using Matlab (\citea{lloyd_representation_2015})}
\label{tab:bexpmat}
\bgroup
\def\arraystretch{1.5}
\begin{tabular}{|l|c|c|c|}
\hline
\multicolumn{1}{|c|}{\textbf{Dataset}} & \textbf{NLL} & \textbf{BIC} & \textbf{\# parameters} \\ \hline
01-airline & 536.4502 & 1117.6287 & 9 \\ \hline
06-internet & 9526.0615 & 19169.5548 & 17 \\ \hline
07-call-centre & 773.8376 & 1635.9554 & 17 \\ \hline
08-radio & 236.5201 & 522.3659 & 9 \\ \hline
\end{tabular}
\egroup
\end{table}

\subsection{Analysis}
Model likelihoods and BIC scores as per Table \ref{tab:bexpoct} and Table \ref{tab:bexpmat} looks almost similar, there is some difference in number of kernel parameters learned for each dataset in both the cases. This is not concerning, as the final model may sometimes include an artefact of search procedure \citea{lloyd_representation_2015}. Scatter plots, Figure \ref{fig:bexpbic} (a) and Figure \ref{fig:bexpbic} (b), show a clear correlation between BIC and Likelihood, this is due to the reason, BIC score is derived from likelihood estimate of Gaussian process model.

Overall, these results suggest that there maybe no impact on using GNU Octave instead of Matlab to interface with GPML toolbox inside ABCD system. These results also confirm that the system has been properly configured on NeSI platform. On the basis of, results of these baseline experiments, further experimentation was performed.

\section{Experiments with Hybrid SoD method}\label{sec:exphybsod}
We performed these experiments to evaluate, the amount of variation in the prediction accuracy, when model is discovered using, Hybrid SoD model evaluation method with a model search strategy other than greedy and model scoring criterion different from BIC.

In our experiments we chose, a wider search strategy like top-k (see Section \ref{sec:algsearchtopk}) and model scoring criterion that considers size of training data such as AIC (see Equation \ref{eq:aicc}). Combinations of model search strategy (greedy and top-k) and model scoring criterion (BIC and AIC), resulted in four set of experiments (see Table \ref{tab:experiments}). This is often referred as ``balanced design'' experiments.

We analyse the results together of both the experiments --- Hybrid SoD (this section) and CV SoD (Section \ref{sec:expcvsod}), in Section \ref{sec:anova}.

\begin{figure}[h]
\centering
\subfigure[\textit{BIC score comparison} ]
{\includegraphics[width=.9\textwidth]{figures/base_results_bic}}
\subfigure[\textit{Likelihood comparison} ]
{\includegraphics[width=.9\textwidth]{figures/base_results_nll}}
\subfigure[\textit{Kernel parameters comparison} ]
{\includegraphics[width=.9\textwidth]{figures/base_results_params}}
\caption{Scatter plots of baseline experiments}
\label{fig:bexpbic}
\end{figure}

\subsection{Results}
All four set of experiments used the same system parameters described in Section \ref{sec:hybSoD}. In these experiments the model was searched for three levels in depth on 13 univariate and four multivariate real-world datasets. We repeated each set of experiments five times, every time with a different random number generator seed. We trained the model on 90\% of the data and used remaining 10\% to perform predictions. 

In every experiment, for each dataset, we computed mean-squared-errors (MSE) as per Equation \ref{eq:mse} then we standardised MSE as per Equation \ref{eq:smse}. SMSE allows us to compare prediction error of all datasets for each experiment. As experiments were repeated, we had five SMSE values for each dataset on every method, we averaged them to see their variability. Table \ref{tab:hybsod} is reporting average of SMSE $\pm$ one unit standard deviation, and is visualised in Figure \ref{fig:expnocv1} the error bars represent the unit standard deviation. 



On some datasets all the methods performed equally well, in fact these methods had a very low SMSE prediction error\footnote{We have rounded SMSE and standard deviations up to four decimal places, due to this some values appear as $0.000$ (in reality they are very small values close to zero), for example standard deviations for datasets 14-quebec-xl, 15-concrete, 16-parkinsons for Top-k AIC or BIC experiments in Table \ref{tab:hybsod}.}, for example, 02-solar, 16-parkinsons. We attribute this behaviour to very low variance in the dataset itself and inclusion of White Noise ($\kWN$) kernel in base kernels (see Appendix \ref{app:withWN}, Section 3.4). 

\bgroup
\def\arraystretch{1.5}
\begin{longtable}{|c|c|c|c|c|c|c|c|c|}
\caption{Results of experiments with Hybrid SoD model evaluation method}\label{tab:hybsod}\\
  \hline
\textbf{Dataset} & \textbf{Greedy AIC} & \textbf{Top-k AIC} & \textbf{Greedy BIC} & \textbf{Top-k BIC}\\ 
  \hline
\endfirsthead
\multicolumn{4}{c}%
{\tablename\ \thetable\ -- \textit{Continued from previous page}} \\
  \hline
\textbf{Dataset} & \textbf{Greedy AIC} & \textbf{Top-k AIC} & \textbf{Greedy BIC} & \textbf{Top-k BIC} \\ 
  \hline
\endhead
\hline \multicolumn{7}{r}{\textit{Continued on next page}} \\
\endfoot
\hline
\endlastfoot
\cline{1-9}\multirow{ 1 }{*}{ 01-airline } & 0.0034 $\pm$ 0.0008 & 0.0061 $\pm$ 0.0029 & 0.0164 $\pm$ 0.0261 & 0.0077 $\pm$ 0.0112 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 02-solar } & 0.0000 $\pm$ 0.0000 & 0.0000 $\pm$ 0.0000 & 0.0000 $\pm$ 0.0000 & 0.0000 $\pm$ 0.0000 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 03-mauna } & 0.0059 $\pm$ 0.0070 & 0.0036 $\pm$ 0.0003 & 0.1777 $\pm$ 0.3083 & 0.0039 $\pm$ 0.0036 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 04-wheat } & 0.1754 $\pm$ 0.1419 & 0.3142 $\pm$ 0.0424 & 0.1997 $\pm$ 0.1460 & 0.3130 $\pm$ 0.0135 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 05-min-temp } & 0.0587 $\pm$ 0.0203 & 0.0697 $\pm$ 0.0275 & 0.0704 $\pm$ 0.0225 & 0.0491 $\pm$ 0.0009 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 06-internet } & 0.2690 $\pm$ 0.2161 & 0.1419 $\pm$ 0.1522 & 0.3852 $\pm$ 0.4082 & 0.0683 $\pm$ 0.0428 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 07-calls } & 1.4179 $\pm$ 0.9368 & 1.2571 $\pm$ 0.3906 & 0.6257 $\pm$ 1.0106 & 1.8601 $\pm$ 0.1097 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 08-radio } & 0.0435 $\pm$ 0.0462 & 0.0656 $\pm$ 0.0009 & 0.0551 $\pm$ 0.0592 & 0.0590 $\pm$ 0.0129 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 09-gas } & 0.4508 $\pm$ 0.3091 & 0.4844 $\pm$ 0.0837 & 1.1540 $\pm$ 1.7102 & 0.3197 $\pm$ 0.2253 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 10-sulphuric } & 0.3061 $\pm$ 0.1025 & 0.4242 $\pm$ 0.0794 & 0.2544 $\pm$ 0.0998 & 0.3430 $\pm$ 0.1193 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 11-unemploy } & 0.2478 $\pm$ 0.1901 & 0.1274  $\pm$  0.1517 & 0.0970  $\pm$  0.0705 & 0.0284  $\pm$  0.0253 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 12-quebec } & 0.0238  $\pm$ 0.0142 & 0.2624  $\pm$  0.3533 & 0.0206  $\pm$  0.0103 & 0.0184  $\pm$  0.0072 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 13-wages } & 0.4023  $\pm$  0.0142 & 0.3634  $\pm$  0.0533 & 0.3840  $\pm$  0.0309 & 0.3689  $\pm$  0.0250 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 14-quebec-xl } & 0.0068  $\pm$  0.0002 & 0.0071  $\pm$  0.0000 & 0.0069  $\pm$  0.0002 & 0.0065  $\pm$  0.0003 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 15-concrete } & 0.0182  $\pm$  0.0102 & 0.0106  $\pm$  0.0000 & 0.0130  $\pm$  0.0044 & 0.0107  $\pm$  0.0001 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 16-parkinsons } & 0.0000  $\pm$  0.0000 & 0.0000  $\pm$  0.0000 & 0.0000  $\pm$  0.0000 & 0.0000  $\pm$  0.0000 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 17-winewhite } & 0.0157  $\pm$  0.0000 & 0.0157  $\pm$  0.0000 & 0.0157  $\pm$  0.0001 & 0.0156  $\pm$  0.0000 \\ 
   \hline
 
\end{longtable}
\egroup

   


\section{Experiments with CV SoD method}\label{sec:expcvsod}
We performed these experiments to evaluate, the amount of variation in the prediction accuracy, when model is discovered using, a novel CV SoD model evaluation method as described in Section \ref{sec:algcvSoD}. We used ``balanced design'' experimentation approach i.e. combinations of model search strategy (greedy and top-k) and model scoring criterion (BIC and AIC), resulted in four set of experiments (see Table \ref{tab:experiments}). 

We collectively analyse the results of CV SoD method (this section) and Hybrid SoD method (Section \ref{sec:exphybsod}) experiments in Section \ref{sec:anova}.

\subsection{Results}
All four set of experiments used the same system parameters described in Section \ref{sec:cvSoD}. We repeated each set of experiments five times, every time with a different random number generator seed. Model was searched for three levels in depth on 13 univariate and four multivariate real-world datasets. Table \ref{tab:cvsod} is reporting average of SMSE $\pm$ one unit standard deviation, for each set of experiment on each dataset and is visualised in Figure \ref{fig:expcv} the error bars represent the unit standard deviation.

We see the similar trend that was seen in experiments with Hybrid SoD method, datasets like 02-solar and 16-parkinsons have very small SMSE prediction error\footnote{Values appearing as $0.000$ in Table \ref{tab:cvsod} are due to rounding of SMSE and standard deviations up to four decimal places. In fact they are very small values close to zero.} across all methods. We associate these trends, to the low variance in datasets and inclusion of White Noise ($\kWN$) kernel in base kernels (see Appendix \ref{app:withWN} Section 3.4).

\bgroup
\def\arraystretch{1.5}
\begin{longtable}{|c|c|c|c|c|c|c|c|c|}
\caption{Results of experiments with CV SoD model evaluation method}\label{tab:cvsod}\\
  \hline
\textbf{Dataset} & \textbf{Greedy AIC} & \textbf{Top-k AIC} & \textbf{Greedy BIC} & \textbf{Top-k BIC} \\ 
  \hline
\endfirsthead
\multicolumn{4}{c}%
{\tablename\ \thetable\ -- \textit{Continued from previous page}} \\
  \hline
\textbf{Dataset} & \textbf{Greedy AIC} & \textbf{Top-k AIC} & \textbf{Greedy BIC} & \textbf{Top-k BIC} \\ 
  \hline
\endhead
\hline \multicolumn{7}{r}{\textit{Continued on next page}} \\
\endfoot
\hline
\endlastfoot
\cline{1-9}\multirow{ 1 }{*}{ 01-airline } & 0.0036 $\pm$ 0.0024 & 0.0017 $\pm$ 0.0010 & 0.0051 $\pm$ 0.0041 & 0.0037 $\pm$ 0.0032 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 02-solar } & 0.0000 $\pm$ 0.0000 & 0.0000 $\pm$ 0.0000 & 0.0000 $\pm$ 0.0000 & 0.0000 $\pm$ 0.0000 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 03-mauna } & 0.0050 $\pm$ 0.0061 & 0.0111 $\pm$ 0.0160 & 0.0022 $\pm$ 0.0009 & 0.0013 $\pm$ 0.0013 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 04-wheat } & 0.2866 $\pm$ 0.0343 & 0.3192 $\pm$ 0.0061 & 0.2471 $\pm$ 0.1368 & 0.3192 $\pm$ 0.0051 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 05-min-temp } & 0.0791 $\pm$ 0.0204 & 0.0619 $\pm$ 0.0232 & 0.0493 $\pm$ 0.0006 & 0.0489 $\pm$ 0.0010 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 06-internet } & 0.3458 $\pm$ 0.3135 & 0.0513 $\pm$ 0.0161 & 0.2385 $\pm$ 0.3105 & 0.3012 $\pm$ 0.2729 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 07-calls } & 2.6567 $\pm$ 1.0172 & 1.7023 $\pm$ 0.3872 & 0.7947 $\pm$ 0.7670 & 2.0362 $\pm$ 0.3961 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 08-radio } & 0.0979 $\pm$ 0.1116 & 0.0971 $\pm$ 0.0180 & 0.0612 $\pm$ 0.0147 & 0.0708 $\pm$ 0.0184 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 09-gas } & 0.4386 $\pm$ 0.3006 & 0.5125 $\pm$ 0.0768 & 0.7894 $\pm$ 0.8900 & 0.3915 $\pm$ 0.0626 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 10-sulphuric } & 0.4670 $\pm$ 0.1453 & 0.2853 $\pm$ 0.1525 & 0.2954 $\pm$ 0.1203 & 0.3551 $\pm$ 0.1506 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 11-unemploy } & 0.2654 $\pm$ 0.3631 & 0.1363 $\pm$ 0.1392 & 0.2414 $\pm$ 0.2484 & 0.1591 $\pm$ 0.2319 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 12-quebec } & 0.0165 $\pm$ 0.0085 & 0.0138 $\pm$ 0.0029 & 0.0152 $\pm$ 0.0077 & 0.0192 $\pm$ 0.0114 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 13-wages } & 0.3793 $\pm$ 0.0498 & 0.4054 $\pm$ 0.0202 & 0.3226 $\pm$ 0.0454 & 0.3929 $\pm$ 0.0084 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 14-quebec-xl } & 0.0066 $\pm$ 0.0003 & 0.0063 $\pm$ 0.0000 & 0.0070 $\pm$ 0.0003 & 0.0071 $\pm$ 0.0000 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 15-concrete } & 0.0120 $\pm$ 0.0031 & 0.0167 $\pm$ 0.0000 & 0.0146 $\pm$ 0.0051 & 0.0108 $\pm$ 0.0000 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 16-parkinsons } & 0.0000 $\pm$ 0.0000 & 0.0000 $\pm$ 0.0000 & 0.0000 $\pm$ 0.0000 & 0.0000 $\pm$ 0.0000 \\ 
  \cline{1-9}\multirow{ 1 }{*}{ 17-winewhite } & 0.0157 $\pm$ 0.0000 & 0.0157 $\pm$ 0.0000 & 0.0156 $\pm$ 0.0000 & 0.0157 $\pm$ 0.0001 \\ 
   \hline
\end{longtable}
\egroup




\section{Result analysis of experiments with Hybrid SoD and CV SoD methods}\label{sec:anova}

In this section, we discuss the results of balanced design experiments performed in Sections \ref{sec:exphybsod} and \ref{sec:expcvsod}. These experiments can also be referred as $2 \times 2 \times 2$ factorial design, since we used two types of model evaluation methods such as Hybrid SoD and CV SoD, two kinds of search strategies, namely, greedy and top-k and two ways to score models i.e. BIC and AIC.

We are validating three hypotheses, described in Section \ref{sec:hypdev}, with the results of these experiments. Hypothesis \ref{hyp:greedy} was developed to test whether composing a covariance kernel in a greedy search approach results in a better performing (or lower SMSE) Gaussian process model. Hypothesis \ref{hyp:bic} was formed to asses whether models penalised by BIC would be able to generalise better (or lower SMSE) on unseen data. Hypothesis \ref{hyp:fit} was created to check whether hybrid subset of data (SoD) approximation to Gaussian process regression provide stable extrapolation (or lower SMSE).

\subsection{Analysis of variance (ANOVA)}
In order to validate these hypotheses, we employed analysis of variances (or ANOVA) method to compare model performance among different set of experiments. ANOVA allows us to check whether these prediction error or SMSE obtained by experiments differ from each other or not. Since error was standardised we considered SMSE of each dataset as a sample drawn from population using a particular method. In this way, we got 17 samples for each experiment (or method). We considered three factors (or independent variables) i.e. evaluation method, search approach and scoring criterion and SMSE as response (or dependent variable).

Figure \ref{fig:boxplots} shows the box plots for each of the three treatments (or factors). From eye-ball analysis, there does not seem to be much difference among them. First step is to check for interaction. An interaction among two factors indicates that different levels of one factor have different impacts (or effects) on different levels of other factor. 

Generally, parallel lines in interaction plots indicate no interaction among factors. Figure \ref{fig:interact} suggests there could be interactions (lines are not parallel) among model scoring criterion and search approach and among model evaluation and search approach. With this informations we performed a three-way ANOVA with interactions between all the factors (see Listing \ref{out:anova}).

\begin{figure}
\centering
\includegraphics[width=.9\textwidth, height=.4\textheight, keepaspectratio]{figures/box_plots}
\caption{Box plots for each level of independent variable}
\label{fig:boxplots}
\end{figure}

\begin{sidewaysfigure}
\centering
\includegraphics[width=.9\textwidth]{figures/exp-nocv-smse}
\caption{Prediction error (SMSE) using Hybrid SoD model evaluation method}
\label{fig:expnocv1}
\end{sidewaysfigure}

\begin{sidewaysfigure}
\centering
\includegraphics[width=.9\textwidth]{figures/exp-cv-smse}
\caption{Prediction error (SMSE) using CV SoD model evaluation method}
\label{fig:expcv}
\end{sidewaysfigure}

\begin{figure} [h]
\centering
\includegraphics[width=.9\textwidth,height=.83\textheight]{figures/interaction_plots}
\caption{Interaction plots of search versus criterion versus evaluation factors}
\label{fig:interact}
\end{figure}
\lstset{basicstyle=\footnotesize\ttfamily,breaklines=true}

\subsubsection*{Three-way ANOVA}
Listing \ref{out:anova} shows a three-way ANOVA performed including interactions with all thee factors. It suggests that there is no strong evidence to reject the null-hypothesis that all group means are same i.e. no method (for example, greedy search using BIC scoring in Hybrid SoD evaluation method) provides better prediction performance over the other (for example, top-k search using AIC scoring in CV SoD evaluation method). It also suggests that there is not evidence about interactions between the factors. 

\begin{minipage}{\linewidth}
{\footnotesize 
\begin{lstlisting}[frame=single,caption=Three-way ANOVA with interactions\label{out:anova}]
> anova(lm(SMSE ~ criterion * search * evaluation, data = fr))
Analysis of Variance Table

Response: SMSE
                             Df  Sum Sq  Mean Sq F value Pr(>F)
criterion                     1  0.0172 0.017238  0.0992 0.7533
search                        1  0.0007 0.000669  0.0039 0.9506
evaluation                    1  0.0293 0.029342  0.1689 0.6818
criterion:search              1  0.0410 0.040982  0.2359 0.6280
criterion:evaluation          1  0.0150 0.015031  0.0865 0.7691
search:evaluation             1  0.0019 0.001917  0.0110 0.9165
criterion:search:evaluation   1  0.0491 0.049118  0.2827 0.5958
Residuals                   128 22.2364 0.173722 
\end{lstlisting}
}
\end{minipage}\\

Moreover, traditional ANOVA has strong assumptions, such as the response should be normally distributed, there should be equal variances among the groups and groups should be independent. Violation of these assumptions could also reflect in non-significant $p\mhyphen values$. 

Based on this analysis, we can consider each experiment as an individual factor and repeated the analysis as single factor with multiple (or repeated) measures.

\subsubsection*{Multivariate ANOVA}
Since, the groups (or samples) are same for each experiment, we employed a multivariate approach to one-way repeated measures analysis of variance (see Listing \ref{out:manova}). Even multivariate ANOVA, did not provide enough evidence to reject the null-hypotheses $(p\mhyphen value (0.311) > 0.05)$. 

\begin{minipage}{\linewidth}
{\footnotesize
\begin{lstlisting}[frame=single,caption=Multivariate ANOVA for repeated measures\label{out:manova}]
> anova(result)
Analysis of Variance Table

            Df  Pillai approx F num Df den Df Pr(>F)
(Intercept)  1 0.49129   1.3796      7     10  0.311
Residuals   16                 

> anova(result, test="Wilks")
Analysis of Variance Table

            Df   Wilks approx F num Df den Df Pr(>F)
(Intercept)  1 0.50871   1.3796      7     10  0.311
Residuals   16                                      
\end{lstlisting}
}
\end{minipage}\\

We then, analysed the trends in Figures \ref{fig:expcv} and \ref{fig:expnocv1}, these figures suggest that the standard deviations of SMSE prediction error is comparatively smaller for most of the datasets using CV SoD method. Also, the report generated by ABCD-SE system (see Appendix \ref{app:withWN} Section 3.4) suggested that noise kernel present in composite kernel explains noise (or variability) of model. 




These reasons motivated us to perform another round of experiments without including White Noise kernel in the base kernels.

\section{Experiments with no White Noise base kernel}
The purpose of these experiments was to check whether the high performance achieved in ``balanced design'' experiments was highly influenced by the presence of White Noise kernel in the base kernels and eventually in discovered structure. Automatic reports generated by ABCD-SE system (such as Appendix \ref{app:withWN}) describe the learnt model, it shows that most of the variability of explained by four base kernel --- $\kSE$, $\kPer$, $\kLin$ and $\kC$ and the remaining is explained by $\kWN$ base kernel.

Thus, we decided to perform these experiments without $\kWN$ kernel. ``Balanced design'' experiments consumed allocated resources on NeSI (CPU core hours = 100,000 hours). We got our allocation extended to perform these last experiments, however we could not perform these in a factorial design, due to resourcing restrictions.

We kept same system parameters as in ``balance design'' experiments, with following exceptions kept search strategy as greedy and model scoring by BIC. We performed these experiments once on seven datasets. Two experiments were executed one with CV SoD model evaluation and other with Hybrid SoD model evaluation method.

\subsection{Results}
Table \ref{tab:wown} presents the prediction error SMSE\footnote{Rounded up to four decimal places.} of learnt models in both experiments. We can see for dataset 02-solar the SMSE is seen as $0.000$ due to rounding, but actual values of SMSE in CV SoD experiment was $\num{0.000000078427}$ and in Hybrid SoD experiment was $\num{0.00000084899}$. Figure \ref{fig:evalrep} shows the scatter plot of the results. ABCD-SE system generated the report for one of the dataset of this experiment (see Appendix \ref{app:woWN}).

\bgroup
\def\arraystretch{1.5}

\begin{longtable}{|c|c|c|}
\caption{Prediction error SMSE of the best models discovered in experiments \\with four base kernels, greedy search and BIC scoring }\label{tab:wown}\\
  \hline
\textbf{Dataset} & \textbf{CV SoD} & \textbf{Hybrid SoD}\\ 
  \hline
\endfirsthead
\multicolumn{3}{c}%
{\tablename\ \thetable\ -- \textit{Continued from previous page}} \\
  \hline
\textbf{Dataset} & \textbf{CV SoD} & \textbf{Hybrid SoD}\\ 
  \hline
\endhead
\hline \multicolumn{3}{r}{\textit{Continued on next page}} \\
\endfoot
\hline
\endlastfoot
\cline{1-3}\multirow{ 1 }{*}{ 01-airline } & 0.0157 & 0.6369 \\ 
  \cline{1-3}\multirow{ 1 }{*}{ 02-solar } & 0.0000 & 0.0000 \\ 
  \cline{1-3}\multirow{ 1 }{*}{ 04-wheat } & 0.2844 & 0.9800 \\ 
  \cline{1-3}\multirow{ 1 }{*}{ 05-min-temp } & 0.0493 & 0.0906 \\ 
  \cline{1-3}\multirow{ 1 }{*}{ 08-radio } & 0.0460 & 0.4180 \\ 
  \cline{1-3}\multirow{ 1 }{*}{ 10-sulphuric } & 0.4849 & 0.9178 \\ 
  \cline{1-3}\multirow{ 1 }{*}{ 13-wages } & 0.2624 & 0.2794 \\ 
   \hline 
\end{longtable}
\egroup

\begin{figure} [h]
\centering
\includegraphics[width=.9\textwidth,height=.25\textheight]{figures/eval_rep}
\caption{Prediction error (SMSE) of models constructed using four base kernels --- $\kSE$, $\kPer$, $\kLin$ and $\kC$, in a greedy search strategy and selected based on highest BIC model score}
\label{fig:evalrep}
\end{figure}


\subsection{Analysis}
We start our analysis of these results by visualising them through a box-plot (see Figure \ref{fig:boxrep}). The box plot suggests that the means could be different i.e. the models evaluated using CV SoD could be performing better (lower SMSE) compared to Hybrid SoD. To check this, we performed a paired t-test and confirmed it with traditional one-way ANOVA (see Listing \ref{out:evalrep}).

\begin{figure} [h]
\centering
\includegraphics[width=.9\textwidth, height=.4\textheight, keepaspectratio]{figures/box_plots_rep}
\caption{Box plot of each model evaluation method}
\label{fig:boxrep}
\end{figure}

The paired t-test with $p\mhyphen value (0.016) < 0.05$ and ANOVA with $p\mhyphen value (0.031) < 0.05$ (in Listing \ref{out:evalrep}) provide strong evidence to reject null-hypotheses --- that both sample means are same. In paired t-test, we tested alternate hypotheses as $H_a: \mu_{Hybrid SoD} - \mu_{CV SoD} > 0$, as per strong evidence we can say that models evaluated with Hybrid SoD compared to CV SoD would have higher SMSE i.e. low performance. On average models evaluated with Hybrid SoD would have 0.311 more SMSE that those evaluated with CV SoD. 


\begin{minipage}{\linewidth}
{\footnotesize
\begin{lstlisting}[frame=single,caption=One-way ANOVA and Paired t-test\label{out:evalrep}]
## Paired t-test

data:  as.numeric(com[, 3]) and as.numeric(com[, 2])
t = 2.8026, df = 6, p-value = 0.01553
alternative hypothesis: true difference in means is greater than 0
95 percent confidence interval:
 0.09550173        Inf
sample estimates:
mean of the differences 
               0.311445 
## ANOVA
Error: dataset
          Df Sum Sq Mean Sq F value Pr(>F)
Residuals  6  0.836  0.1393               

Error: dataset:evaluation
           Df Sum Sq Mean Sq F value Pr(>F)  
evaluation  1 0.3395  0.3395   7.854 0.0311 *
Residuals   6 0.2593  0.0432                 
---
Signif. codes: 0 `***' 0.001 `**' 0.01 `*' 0.05 `.' 0.1 ` ' 1

\end{lstlisting}
}
\end{minipage}


\section{Discussion}
Through this dissertation, we were able to experiment several key areas of ABCD system like model search, scoring and evaluation. Baseline experiments provided a go-ahead with GNU Octave as the results were very similar. We then experimented in a factorial design way to validate three hypotheses. Experiments with Hybrid SoD and CV SoD methods could not provide enough statistical evidence to reject Hypothesis \ref{hyp:greedy}, \ref{hyp:bic} and \ref{hyp:fit}. 

ANOVA analysis suggested that the group means are not different for different methods. By analysing generated reports and error bar plots we design a new set of experiment that excluded $\kWN$ kernel from base kernels. These experiments provided strong statistical evidence to reject null-hypotheses that group means are same for model evaluated with Hybrid SoD and CV SoD. With paired t-test it was confirmed that models evaluated with Hybrid SoD, would on average have 0.311 higher SMSE (low predictive accuracy) than models evaluated with CV SoD method. We need further experimentation to support this claim that CV SoD provides stable extrapolation. Perhaps a factorial design experiment would be needed to confirm this, with at least five repetitions.


\bgroup
\def\arraystretch{1.5}
\begin{longtable}{|c|c|c|c|c|c|c|c|c|}
\caption{Average number of models searched for each dataset,\\ in all of the experiments, that searched three levels deep}\label{tab:avgnoofmodels}\\
  \hline
\textbf{Dataset} & \textbf{Greedy} & \textbf{Top-k} & \textbf{\% increase}  \\ 
  \hline
\endfirsthead
\multicolumn{4}{c}%
{\tablename\ \thetable\ -- \textit{Continued from previous page}} \\
  \hline
\textbf{Dataset} & \textbf{Greedy} & \textbf{Top-k} & \textbf{\% increase}  \\ 
  \hline
\endhead
\hline \multicolumn{7}{r}{\textit{Continued on next page}} \\
\endfoot
\hline
\endlastfoot
\cline{1-4}\multirow{ 1 }{*}{ 01-airline } & 1231 & 3811 & 209.66 \\ 
  \cline{1-4}\multirow{ 1 }{*}{ 02-solar } & 1272 & 3825 & 200.71 \\ 
  \cline{1-4}\multirow{ 1 }{*}{ 03-mauna } & 1238 & 3576 & 188.82 \\ 
  \cline{1-4}\multirow{ 1 }{*}{ 04-wheat } & 1107 & 3278 & 196.14 \\ 
  \cline{1-4}\multirow{ 1 }{*}{ 05-min-temp } & 1238 & 3574 & 188.61 \\ 
  \cline{1-4}\multirow{ 1 }{*}{ 06-internet } & 1215 & 3669 & 202.04 \\ 
  \cline{1-4}\multirow{ 1 }{*}{ 07-calls } & 1220 & 3804 & 211.68 \\ 
  \cline{1-4}\multirow{ 1 }{*}{ 08-radio } & 1224 & 3804 & 210.78 \\ 
  \cline{1-4}\multirow{ 1 }{*}{ 09-gas } & 1227 & 3705 & 202.04 \\ 
  \cline{1-4}\multirow{ 1 }{*}{ 10-sulphuric } & 1231 & 3849 & 212.74 \\ 
  \cline{1-4}\multirow{ 1 }{*}{ 11-unemploy } & 1170 & 3805 & 225.23 \\ 
  \cline{1-4}\multirow{ 1 }{*}{ 12-quebec } & 1245 & 3772 & 202.97 \\ 
  \cline{1-4}\multirow{ 1 }{*}{ 13-wages } & 1244 & 3536 & 184.23 \\ 
  \cline{1-4}\multirow{ 1 }{*}{ 14-quebec-xl } & 4238 & 12066 & 184.71 \\ 
  \cline{1-4}\multirow{ 1 }{*}{ 15-concrete } & 7189 & 22650 & 215.05 \\ 
  \cline{1-4}\multirow{ 1 }{*}{ 16-parkinsons } & 15973 & 39020 & 144.28 \\ 
  \cline{1-4}\multirow{ 1 }{*}{ 17-winewhite } & 9187 & 29184 & 217.67 \\ 
\hline 
\end{longtable}
\egroup


We analysed the number of models searched for each dataset in every experiment. Since each experiment was repeated five times, we took the average number of models or covariance structures search using greedy approach and through top-k search and tabulated them data in Table \ref{tab:avgnoofmodels}. On average, we saw the increase of $\approx200\%$ in number of models searched between the two methods. Top-k searches a much wider model space. The number of models searched are linearly proportional to number of dimensions in (or features of) input $\inputVar$. We can observe it when we compare univariate versus multivariate datasets. For that reason, top-k approach can potentially discover richer structures. In order to truly see the effectiveness of top-k search procedure, we believe, we need to perform experiments without $\kWN$ kernel. 

With respect to model criterion, changing it to AIC may not result in much difference in model performance as both AIC and BIC are derived from likelihood. The likelihood is dependent upon number of kernel parameters. Thus, in our opinion rather than penalising a model for high number of kernel parameters, if we try to optimise or select fewer parameters during model building that would result in much better extrapolation.

In next section, we reflect back to our contributions, discuss limitations of our approaches and briefly describe future research areas.