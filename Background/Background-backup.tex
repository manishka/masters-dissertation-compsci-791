\chapter{Background} \label{chapt:Background}

In this section we will introduce Bayesian non parametric modelling using Gaussian processes and then discuss the concept of covariance kernel with its importance in Gaussian process models. 
In the second half of this section we will describe an existing automatic system for data analysis, its essential elements and procedures. 
Towards the end, we will discuss the limitations of existing automatic system and briefly propose the changes we wish to experiment in this dissertation.

\section{Modelling functions using Gaussian Processes}
Gaussian processes have been introduced in many ways by several authors in literature. The introduction that is most relevant to this dissertation is given by \citea{rasmussen_gaussian_2006}. We would like to start from linear regression and slowly progress towards Gaussian processes. This section is adapted from \citea{lloyd_representation_2015}.

In a simple form of Bayesian linear regression model, we would have a training data $D$ that contains input and output pairs as, 
\begin{equation}
D = \{(x_i,y_i) \in \mathbb{R}^2: i = 1,..,N\}
\end{equation}
A simple probabilistic linear regression model for $D$ could be represented as,

\begin{equation}\label{eq:prior_y} 
y_i \sim mx_i + \epsilon_i
\end{equation}
\begin{equation} \label{eq:prior_m}
m \sim \mathcal{N}(0,1)
\end{equation}
\begin{equation}\label{eq:prior_eps}
\epsilon_i \overset{iid}{\sim} \mathcal{N}(0,\sigma_\epsilon^2)
\end{equation}
Equations \ref{eq:prior_y}, \ref{eq:prior_m} and \ref{eq:prior_eps} represents our belief that output $y_i$ is linearly related to input $x_i$ and a line passing through origin depicts that relationship.

Our beliefs would update once we see the data $D$, as per probability theory we would find updates as,

\begin{equation}
y_i|D \sim \mathcal{N}\left(\frac{x_j\sum_i x_iy_i}{\sum_i x_i^2 + \sigma_\epsilon^2}, \frac{x_j^2\sigma_\epsilon^2}{\sum_i x_i^2 + \sigma_\epsilon^2} + \sigma_\epsilon^2\right)
 \quad j \not\in \{1,..,N\}
\end{equation}

\begin{equation}\label{eq:post_m}
m|D \sim \mathcal{N}\left(\frac{\sum_i x_iy_i}{\sum_i x_i^2 + \sigma_\epsilon^2},\frac{\sigma_\epsilon^2}{\sum_i x_i^2+\sigma_\epsilon^2}\right)
\end{equation}
Figure \ref{fig:baye_lin_reg} shows the updates of prior belief for $m$ (Equation \ref{eq:prior_m}) into posterior belief (Equation \ref{eq:post_m}). As more and more data is observed (or seen) the posterior belief on parameter (or slope) $m$ gets more concentrated. 
\begin{figure}[h]
\subfigure[Prior belief]{
\includegraphics[width=.3\textwidth]{figures/prior}
}
\subfigure[Posterior after 5 samples]{
\includegraphics[width=.3\textwidth]{figures/bayes_5}
}
\subfigure[Posterior after 15 observations]{
\includegraphics[width=.3\textwidth]{figures/bayes_15}
}
\caption{Bayesian linear regression, lines are random samples from prior probability distribution, the red shading represents the probability distribution.   
\newline (a): Represents prior distribution on slope (or gradient) $m$ \cite{lloyd_representation_2015}. \newline (b): Represents the posterior distribution on slope $m$, after seeing 5 samples \cite{lloyd_representation_2015}.
\newline (c): Represents the posterior distribution on slope $m$, after 15 observations \cite{lloyd_representation_2015}.}
\label{fig:baye_lin_reg}
\end{figure}

In order to represent linear relationship between $y = (y_i)$ and $x = (x_i)$ as a noisy relationship, we can rewrite these Equations as (\ref{eq:noisy}, \ref{eq:noisy2}).

\begin{equation} \label{eq:noisy}
y \sim \mathcal{N}(0,x_i^2+\sigma_\epsilon^2)
\end{equation}
\begin{equation}\label{eq:noisy2}
\text{Cov}(y_i, y_j) = x_ix_j \quad \forall i \neq j
\end{equation}
Now, if we assume the prior distribution of $y$ to be a multivariate Gaussian distribution (Equation \ref{eq:multiGauss}), we get,
\begin{equation}\label{eq:multiGauss}
y\sim \mathcal{N}(0,K) \quad  \text{where} \quad  K_{ij} = x_ix_j + \delta_{ij}\sigma_\epsilon^2
\end{equation}
In Equation \ref{eq:multiGauss}, $\delta_{ij}$ is the Kronecker delta. When we represent covariance $K_{ij}$ as a function of input pairs $k(x_i, x_j)$ we get,
\begin{equation}\label{eq:covfunc}
K_{ij} = \kernel(x_i, x_j) = x_ix_j + \delta_{x_ix_j}\sigma_\epsilon^2
\end{equation}
Equation \ref{eq:covfunc} is true for all datasets of any finite size. We can also define a probability distribution over $y$ when $x$ is infinite ($x = \mathbb{R}$). 

In a collection of random variables, when any finite subset have a joint Gaussian distribution, that collection is referred as a Gaussian process. To specify a Gaussian process, we need to specify the mean and covariance of any finite subset of random variables. Mean can be represented by a mean function $\mu : \mathcal{X} \rightarrow \mathbb{R}$ and covariance can be represented by a covariance function or kernel $k : \mathcal{X}^2 \rightarrow \mathbb{R}$, where $\mathcal{X}$ is the input space $x$, if we substitute mean and covariance functions in Equations \ref{eq:noisy} and \ref{eq:noisy2} we get,
\begin{equation}
y_i \sim \mathcal{N}(\mu(x_i), \kernel(x_i,x_j))
\end{equation}
\begin{equation}
\mathbb{E}(y_i) = \mu(x_i)
\end{equation}
\begin{equation}
\text{Cov}(y_i,y_j) = \kernel(x_i,x_j)
\end{equation}
or 
\begin{equation} \label{eq:fx}
(y_i : i = 1,...,N) \sim \mathcal{N}\left((\mu(x_i) : i = 1,...,N), (\kernel(x_i, x_j) : i,j = 1,...,N)\right)
\end{equation}
If we replace $y_i = f(x_i)$ in Equation \ref{eq:fx}, we would define a probability distribution over functions $f$. However, matrix $K = \kernel(x_i,x_j)$ must be a positive semi-definite, only then we can define a valid probability distribution. This brings us to another definition of Gaussian processes, they are the probability distributions over functions, for any finite subset of random variables that have a joint Gaussian distribution and are denoted as,
\begin{equation}
f \sim \gp{}(\mu,k)
\end{equation}

\subsubsection{What is a covariance kernel?}
The kernel $\kernel$ directly specifies the covariance (or similarity) between a pair of random function values (or output points) at a pair of input points: $\kernel(\inputVar_i, \inputVar_j) = \text{Cov}(f(\inputVar_i), f(\inputVar_j))$. A covariance kernel therefore encodes inductive biases - what sorts of solution functions we expect. By choosing a covariance function, we choose whether the solution functions are periodic, smooth, linear, polynomial and so on. In other words, we hardly need to modify our algorithm to radically change the model - we just change the covariance function.

We have so far used the terms \textit{kernel, covariance kernel} and \textit{covariance function} interchangeably. In general, a \textit{kernel} is a function that maps any pair of inputs into $\mathbb{R}$. The covariance function of a Gaussian process is an example of a kernel. For $\kernel(\inputVar_i,\inputVar_j)$ to be a valid covariance function of a Gaussian process, any matrix $K$ with elements $K_{ij} = \kernel(\inputVar_i, \inputVar_j)$ must be positive semi-definite; this requirement follows since the covariance matrix in a Gaussian distribution must be positive semi-definite.

\subsubsection{Encoding inductive biases}
Let us now explore the properties of Gaussian process i.e. probability distribution over functions by changing $\mu$ and $\kernel$. The mean function $\mu$ provides pointwise mean of the distribution over functions. The kernel function $\kernel$ controls the shape of the sample function that can be drawn from this distribution over functions. It is interesting to note, the changes in the patterns of random samples, drawn from Gaussian process that have different kernel functions. Figure \ref{fig:GP_diff_kernel}, for instance, shows random sampled functions drawn from a zero mean Gaussian process with linear, smooth and periodic kernel functions.

\begin{figure}[h]
\subfigure[\textit{Linear functions} ]{
\includegraphics[clip, trim=1.5cm 10.5cm 9cm 1cm,width=.3\textwidth]{figures/lin_fun_gp}
}
\subfigure[\textit{Smooth slow varying functions} ]{
\includegraphics[clip, trim=1.5cm 10.5cm 9cm 1cm,width=.3\textwidth]{figures/smo_fun_gp}
}
\subfigure[\textit{Smooth periodic functions} ]{
\includegraphics[clip, trim=1.5cm 10.5cm 9cm 1cm,width=.3\textwidth]{figures/per_fun_gp}
}
\caption{Random samples from zero mean Gaussian process with different covariance functions. Lines are random samples drawn from Gaussian process distribution, the red shading represents the pointwise probability distribution.}
\label{fig:GP_diff_kernel}
\end{figure}

Kernel function used in Figure \ref{fig:GP_diff_kernel}(a) was same as Equation \ref{eq:covfunc} with noise variance equal to zero, as shown below,
\begin{equation}
\kernel(x_i,x_j) = x_ix_j
\end{equation}

Kernel function used in Figure \ref{fig:GP_diff_kernel}(b) was of form
\begin{equation}
\kernel(x_i,x_j) = \exp(-\alpha|x_i-x_j|^2) \quad \text{where} \quad \alpha \quad \text{is a constant}
\end{equation}
In this distribution, when distance between $x_i$ and $x_j$ is small the covariance (and correlation) between $f(x_i)$ and $f(x_j)$ will be approximately equal to 1. As the distance $|x_i - x_j|$ increases the correlation decreases monotonically to zero. This decreasing correlation determines the smoothness characteristic of the random sampled function in Figure \ref{fig:GP_diff_kernel}(b).

Lastly, the kernel function used in Figure \ref{fig:GP_diff_kernel}(c) was of the form,
\begin{equation}
\kernel(x_i,x_j) = \exp \left(-\frac{2\sin^2(\pi(x_i-x_j)/p)}{l^2}\right)
\end{equation}
where $p$ and $l$ are constants. In this distribution when $|x_i - x_j|$ is an integer multiple of $p$, the correlation is equal to 1, this results in exact periodic characteristic of random sampled function in Figure \ref{fig:GP_diff_kernel}(c).\\

In summary, a Gaussian process is a collection of random variables, within which a finite set of variables have a joint Gaussian distribution. A Gaussian process can be completely specified by its mean and kernel functions. A mean function represents the estimation of input function, whereas kernel function represents similarity between input pairs. By choosing a different kernel function, we can model different pattern of training data. We can even combine several kernel functions together to model complex but interpretable patterns of data. This property allows us to create different probabilistic models by specifying different kernels or combinations of them. However, choosing a kernel function requires extensive human expertise and is often time consuming as it follows trail and error approach. Owing to these reasons, the use of Gaussian processes is very limited. In the next section, we discuss how ABCD system overcomes these limitations by automatically learning different combinations of kernel functions that best describe the given dataset.

\section{ABCD: A system for automatic data analysis}
Automatic Bayesian Covariance Discovery (ABCD) system is designed to automatically learn a Gaussian process model with zero mean and a composite kernel. The GP model is searched in a open-ended model space using greedy search approach. Figure \ref{fig:abcdflowchart} shows a high level flow chart of the system. Ellipses represent input to and output from various procedures. Rounded rectangles represent the procedures. This system is divided into two stages - model search and report generation. 

\begin{figure} [h]
\centering
\input{figures/flowchart}
\caption{Flow chart of Automatic Bayesian Covariance Discovery (ABCD) system \cite{lloyd_automatic_2014}.}
\label{fig:abcdflowchart}
\end{figure}

In model construction/ search stage, a open-ended model space is searched, in attempt to best describe or discover patterns from training data using a language of models. The search procedure is iterative and at each level a searched model is evaluated using Bayesian Information Criterion (BIC). The model with optimum BIC score is chosen for next iteration (i.e. search follows a greedy approach).

In report generation stage, the best selected model is used to perform extrapolation or prediction on test data, a Natural Language Processing (NLP) component interprets its composite kernel and converts it into English language statements and a model checking procedure performs model criticism. By combining all these information a report is generated in English language that describes the model and its performance.

This dissertation focusses on model search and model evaluation procedures. We therefore paint the components that are not part of this dissertation in grey colour, such as model checking, description and report generation (Figure \ref{fig:abcdflowchart}).

\subsection{Zero Mean Gaussian Process}
Discovering patterns in given dataset automatically using Gaussian processes would  require a language of models, a model search procedure and a model evaluation framework. 

Since, a Gaussian process is completely determined by its mean and kernel function,
\begin{equation}
\{f(x) | a \sim \gp{}(a \times \mu(x_i), \kernel(x_i,x_j))\} \quad \text{where} \quad a \sim \mathcal{N}(0,1)
\end{equation}

If mean is marginalised out, we get a zero mean Gaussian process as shown below,
\begin{equation}
f(x) \sim \gp{}(0, \mu(x_i)\mu(x_j) + \kernel(x_i,x_j))
\end{equation}

In a zero mean Gaussian process, the structure of the kernel describes high level
characteristics of the unknown function, $f$, which eventually
determines how the model generalizes or extrapolates to new
data. Using a zero mean Gaussian processes, we can therefore define a language for Gaussian process regression models as `language of kernels'.


\subsection{Atoms of kernel language}
\begin{figure}[h]
\input{figures/atomskl}
\caption{Five base kernels \cite{lloyd_james_2014}.}
\label{fig:atomskl}
\end{figure}


This language has a set of five base kernels, also referred as the basic elements. Each base kernel function captures a different function property. When we create a composite kernel by combining these base kernels we still get a valid kernel i.e. it satisfies requirement of positive semi-definite \cite{lloyd_automatic_2014}. These base kernels are shown in Figure \ref{fig:atomskl}, they are squared exponential ($\kSE$), periodic ($\kPer$), linear ($\kLin$), constant ($\kC$) and the white noise ($\kWN$). Base kernels are defined as follows, these equations are applicable to scalar-valued inputs:
\begin{eqnarray}
\kSE(\inputVar_i, \inputVar_j) =& \sigma^2\exp\left(-\frac{(\inputVar_i - \inputVar_j)^2}{2\ell^2}\right) \\
\kPer(\inputVar_i, \inputVar_j) =&  \sigma^2\frac{\exp\left(\frac{\cos\frac{2 \pi (\inputVar_i - \inputVar_j)}{p}}{\ell^2}\right) - I_0\left(\frac{1}{\ell^2}\right)}{\exp\left(\frac{1}{\ell^2}\right) - I_0\left(\frac{1}{\ell^2}\right)} \\
\kLin(\inputVar_i, \inputVar_j) =& \sigma^2(\inputVar_i - \ell)(\inputVar_j - \ell) \\
\kC(\inputVar_i, \inputVar_j) =& \sigma^2 \\
\kWN(\inputVar_i, \inputVar_j) =& \sigma^2\delta_{\inputVar_i, \inputVar_j}
\end{eqnarray}

where $l$ and $p$ are parameters\footnote{Also known as $hyperparameters$.} of the kernel functions, $\delta_{\inputVar_i, \inputVar_j}$ is the Kronecker delta function, $I_0$ is the modified Bessel function of the first kind of order zero.


These base kernels - squared exponential ($\kSE$), periodic ($\kPer$), linear ($\kLin$), constant ($\kC$) and the white noise ($\kWN$) encode for smooth functions, periodic functions, linear functions, constant functions, and uncorrelated noise respectively. These encodings are shown in Figure \ref{fig:atomsfunc}.

\begin{figure}[h]
\input{figures/atomsfunc}
\caption{Functions encoded by five base kernels \cite{lloyd_james_2014}.}
\label{fig:atomsfunc}
\end{figure}

\subsection{Composition rules of kernel language} \label{Background:Rules}
There are two main composition rules of kernel language, they are addition and multiplication. Summing two kernels together can be viewed as OR'ing them, since two input points are considered similar if either kernel has a high value in covariance matrix (Equation \ref{eq:sum}). Similarly, product of two kernels can be viewed as AND'ing them together, since two input points are considered similar only if both kernels have high values in covariance matrix (Equation \ref{eq:prod}).
\begin{align}
(k_1 + k_2)(x,x') = k_1(x,x') + k_2(x,x') \label{eq:sum}\\
(k_1 \times k_2)(x,x') = k_1(x,x') \times k_2(x,x') \label{eq:prod}
\end{align}

When combining base kernels using sum and product operations, yields kernels that encode  richer structures, such as approximate periodicity ($\kSE \times \kPer$) or smooth functions with linear trends ($\kSE + \kLin$), more examples can be seen in Figure \ref{fig:rules}.

\begin{figure}[h]
\input{figures/comp}
\caption{Examples of richer structures using compositional rules \cite{lloyd_james_2014}.}
\label{fig:rules}
\end{figure}

There are two more advanced operators included in language to model changepoints in time-series data. They are changepoint $\kCP(\cdot,\cdot)$ and changewindow $\kCW(\cdot,\cdot)$.  Changepoints are defined by addition and multiplication with sigmoidal functions:
\begin{align}
\kCP(\kernel_1, \kernel_2) = \kernel_1 \times \boldsymbol\sigma + \kernel_2 \times \boldsymbol{\bar\sigma}
\label{eq:cp}
\end{align}
where $\boldsymbol\sigma = \sigma(\inputVar_i)\sigma(\inputVar_j)$ and $\boldsymbol{\bar\sigma} = (1-\sigma(\inputVar_i))(1-\sigma(\inputVar_j))$. Similarly, by replacing $\sigma(\inputVar_i)$ with a product of two sigmoids changewindow $\kCW(\cdot,\cdot)$ operator is defined.

This dissertation does not use  $\kCP(\cdot,\cdot)$ and $\kCW(\cdot,\cdot)$ operators and focusses on the kernels that are composed using $+$, $\times$ and replacement search operators as represented below:
\begin{align}
\subexpr \rightarrow \subexpr + \baseker \label{eq:add}\\
\subexpr \rightarrow \subexpr \times \baseker \label{eq:prod}\\
\baseker \rightarrow \baseker^\prime\label{eq:replace}
\end{align}
where $\subexpr$ represents any kernel subexpression and $\baseker$ and $\baseker^\prime$ are any base kernel. These search operators represent addition (Equation \ref{eq:add}), multiplication (Equation \ref{eq:prod}) and replacement (Equation \ref{eq:replace}). 

In the next section, we discuss how and when these search operators are used in an open-ended model space to search a good model.

\subsection{Discovering composite kernel} \label{Background:Search}
As per our discussion so far, we can say that kernel is the heart of a Gaussian process. It has the potential to greatly change a Gaussian process model, without requiring to change inference or
learning procedures. We have also discussed that using a small number of base kernels compositionally i.e. by adding or multiplying we can construct a variety of sophisticated structures. In particular, ABCD system uses five base kernel families: \kSE, \kPer, \kLin, \kC \space and \kWN. The compositional framework described in Section \ref{litrev:search} and \citea{duvenaud_structure_2013} has been extended and adapted by ABCD system. 

In ABCD system, the base kernels are different and search operators include changepoint and changewindow along with summation and multiplication. Since, this dissertation is only focussing on summation and multiplication operators, we will discuss an example involving these search operators, in the following subsection. 

\begin{algorithm}[h]
\caption{Automatic Bayesian Covariance Discovery}\label{alg:search}
\begin{algorithmic}[1]
\State \multilinestate{$\var{kernels} \gets$ base kernels: \{$\kSE$, $\kPer$, $\kLin$, $\kC$, $\kWN$\}}
\State \multilinestate{$\var{operators} \gets$ \{$+$, $\times$\}}
\State \multilinestate{$\var{criterion} \gets$ $\textrm{BIC}$}
\State \multilinestate{$\var{best\_Model} \gets$ \Call{Perform\_Kernel\_Search}{$\var{kernels}$, $\var{operators}$, $\var{criterion}$}}
\State \multilinestate{$\var{ypred} \gets$ \Call{Make\_Predictions}{$\var{best\_Model}$, $\var{xtest}$}}
\end {algorithmic}
\end{algorithm}

At a high level, to discover a composite kernel and measure its performance ABCD system performs three steps as formalised in Algorithm \ref{alg:search}. In the first step necessary variables, like base kernels, operators, criterion used for model evaluation are initialised. The kernel search is performed next, this step discovers the composite kernel using Gaussian process regression that best describes the given training dataset. Algorithms \ref{alg:kernsearch} and \ref{alg:modeleval} formalise the steps involved in kernel search and evaluation. Finally, ABCD system performs prediction on test data to verify how well the discovered model generalises on unseen data. 

As the focus of this dissertation is on model search, evaluation and criterion we have provided algorithms related to these areas of ABCD system and explain them in following section with a help of an example.



\subsubsection{A working example}
Consider a training data $D=\{(\inputVar_i,\outputVar_i)\}_{i=1}^{N}$, where $\inputVar_i$ is a vector of size $d$ representing number of input dimensions or features. The search procedure begins by applying all base kernels to all input dimensions (see Figure \ref{fig:searchtree1} and steps 2 to 4 in Algorithm \ref{alg:kernsearch}). 

\begin{figure}[h]
\centering
\begin{tikzpicture}
[sibling distance=0.18\columnwidth,-,thick, level distance=0.13\columnwidth]
\node[shape=rectangle,draw,thick] {Start}
  child {node {$\kSE$}}
  child {node[shape=rectangle,draw,thick,fill=green!45] {$\kPer$}}
  child {node {$\kLin$}}
  child {node {$\kC$}}
  child {node {$\kWN$}}
  ;
\end{tikzpicture}
\caption{Covariance function discovery using greedy search approach at depth level 1}
\label{fig:searchtree1}
\end{figure}

Once initial set of GP models are proposed, search proceeds to model fitting and evaluation (steps 5 to 8 in Algorithm \ref{alg:kernsearch} and entire Algorithm \ref{alg:modeleval}). The kernel parameters, $\theta$, of each of these proposed GP models are optimised using conjugate gradient descent \cite{rasmussen_gaussian_2006}. Then, each proposed model, $M$, is evaluated using marginal likelihood (or model evidence) $p(D|M)$, that is computed analytically \cite{rasmussen_gaussian_2006} as shown below,
\begin{align}
p(D|M) = \int{p(D|\theta_{opt},M)p(\theta_{opt}|M)d\theta_{opt}}  
\end{align}
where $\theta_{opt}$ is optimised kernel parameters. After evaluation, the marginal likelihood, $p(D|M)$, is penalised for the optimised kernel parameters, $\theta_{opt}$, using Bayesian Information Criterion
(BIC) \cite{schwarz_estimating_1978} as shown below,
\begin{align}\label{eq:bic}
\textrm{BIC}(M) = {-2\log p(D|M)} - {p \space \log N}
\end{align}
where $p$ is the number of kernel parameters. The value of $\textrm{BIC}(M)$ becomes the score of model, $M$ (step 7 in Algorithm \ref{alg:kernsearch}). The model with highest score (i.e. lowest $\textrm{BIC}(M)$ value) is selected as best model for current depth level (the best model is shown in a green box in Figure \ref{fig:searchtree1} and step 9 in Algorithm \ref{alg:kernsearch}). If the desired depth is not reached, then the search continues a level deeper (steps 10 to 14 in Algorithm \ref{alg:kernsearch}).


\begin{algorithm}[h]
\caption{Automatic Bayesian Covariance Discovery: Kernel Search}\label{alg:kernsearch}
\begin{algorithmic}[1]
\Procedure{Perform\_Kernel\_Search}{$\var{kernels}$, $\var{operators}$, $\var{criterion}$}
\State \multilinestate{$\var{\{M\}} \gets$ \Call{Create\_GP\_Models}{\var{kernels}}}
\State \multilinestate{$\var{\{M\}} \gets$ \Call{Random\_Restart\_Covariance\_Functions}{$\var{M}$}}\label{marker}
\State \multilinestate{$\var{\{M\}} \gets$ \Call{Remove\_Duplicate\_Models}{$\var{M}$}}
\For{\texttt{\var{model} in \var{\{M\}}}}
	\State \multilinestate{$\var{likelihoods[model]} \gets$ \Call{Evaluate\_Model}{$\var{model}$, $\var{xtrain}$, $\var{ytrain}$}}
	\State \multilinestate{$\var{scores[model]} \gets$ \Call{Calculate\_Score}{$\var{likelihoods[model]}$, $\var{criterion}$}}
\EndFor
\State \multilinestate{$\var{best\_model} \gets$ \Call{Highest\_Score}{$\var{scores}$, $\var{criterion}$}}
\If {$\var{depth} \leq \var{desired\_depth}$ \quad \textbf{and} \newline \qquad \Call{Is\_Model\_Score\_Improved}{$\var{scores[best\_model]}$, $\var{threshold}$}}
    \State $\var{depth} \gets \var{depth} + 1$
    \State \multilinestate{$\var{\{M\}} \gets$ \Call{Expand\_Kernels}{\var{M[best\_model]}, \var{kernels}, \var{operators}}}
    \State \Goto{marker}
\EndIf
	\State \textbf{return} $\var{M[best\_model]}$
\EndProcedure
\end {algorithmic}
\end{algorithm}

 In next level, the best GP model is expanded by applying all possible search operators (Equations \ref{eq:add}, \ref{eq:prod} and \ref{eq:replace}) and a new set of proposed models are created (see Figure \ref{fig:searchtree2}). Again, each new proposed model is evaluated, scored and the model with highest score is selected as best model for current depth level (the best model is shown in a green box in Figure \ref{fig:searchtree2}).

\begin{algorithm}[h]
\caption{Automatic Bayesian Covariance Discovery: Model Evaluation }\label{alg:modeleval}
\begin{algorithmic}[1]
\Procedure{Evaluate\_Model}{$\var{model}$, $\var{xtrain}$, $\var{ytrain}$}
      \State \multilinestate{$\var{xsub} \gets$ \Call{Random\_Sample}{$\var{xtrain}$}}
      \State \multilinestate{$\var{ysub} \gets$ \Call{Random\_Sample}{$\var{ytrain}$}}
      \State $\var{iter} \gets \var{iters}$
      \While{$\var{iter}\not=0$}\
      	\State \multilinestate{$\var{theta\_opt} \gets$ \Call{Minimise}{$\var{theta\_opt}$, $\var{xsub}$,$\var{ysub}$}}
        \State $\var{iter} \gets \var{iter} - 1$
        \EndWhile
              \State $\var{full\_iter} \gets \var{full\_iters}$
      \While{$\var{full\_iter}\not=0$}\
      	\State \multilinestate{$\var{theta\_opt} \gets$ \Call{Minimise}{$\var{theta\_opt}$, $\var{xtrain}$,$\var{ytrain}$}}
        \State $\var{full\_iter} \gets \var{full\_iter} - 1$
        \EndWhile
      \State \multilinestate{$\var{likelihood} \gets$ \Call{GP\_Fit}{$\var{theta\_opt}$, $\var{xtrain}$,$\var{ytrain}$}}
      \State \textbf{return} $\var{likelihood}$
    \EndProcedure
\end{algorithmic}
\end{algorithm}



\begin{figure}[h]
\centering
\begin{tikzpicture}
[sibling distance=0.18\columnwidth,-,thick, level distance=0.13\columnwidth]
\node[shape=rectangle,draw,thick] {Start}
  child {node {$\kSE$}}
  child {node[shape=rectangle,draw,thick,fill=green!15] {$\kPer$}
    [sibling distance=0.16\columnwidth]
    child {node {$\kPer + \kSE$}}
    child {node {\ldots}}
    child {node[shape=rectangle,draw,thick,fill=green!45] {$\kPer \times \kSE$}}
    child {node {\ldots}}
    child {node {$\kPer \times \kWN$}}
  }
  child {node {$\kLin$}}
  child {node {$\kC$}}
  child {node {$\kWN$}}
  ;
\end{tikzpicture}
\caption{Covariance function discovery using greedy search approach at depth level 2}
\label{fig:searchtree2}
\end{figure}


Search procedure is stopped, if there is no improvement in model scores beyond certain threshold or desired search depth is reached. This search approach is referred as greedy search and is visualised in Figure \ref{fig:searchtree}, green boxes show models that scored highest at a particular level. After reaching to desired depth in search tree, the best model of that depth level is returned to main procedure (step 15 in Algorithm \ref{alg:kernsearch}). The best model is then used to make predictions about the data (step 5 in Algorithm \ref{alg:search}).


Usually, researchers construct a composite kernel in such iterative way, therefore this simple strategy is employed in ABCD system. Additionally, a previous study has demonstrated empirical performance using such search strategy \cite{grosse_exploiting_2012}.











\begin{figure}[h]
\input{figures/searchtree}
\caption{Covariance function discovery using greedy search approach}
\label{fig:searchtree}
\end{figure}








\section{Summary} \label{Background:Summary}
In this section we described how Gaussian Process regression model can describe a different structure of data, by changing the covariance (or kernel) function, without changing the learning method. We also discussed how ABCD system learns or discovers the composite kernel automatically in a greedy search approach. 

There are, however, certain limitations to the model search approach and model scoring criteria. \citea{lloyd_representation_2015} suggested that use of BIC to approximate model evidence is not contextually justified due to difficulties in finding tractable marginal likelihood. \citea{lloyd_representation_2015} also, argued that optimising over parameters is not a convex optimisation problem, and space could have local optima. Although hot start and random restarts are used, they do not guarantee global optima. \citea{lloyd_representation_2015} mentioned that we would not want to optimise all parameters fully since we have to look at many models and it will be computationally expensive to fully optimise all parameters on full training data for each model.

This dissertation proposes several techniques in attempt to overcome these limitations, they are discussed in detail in section \ref{chapt:ExpDes}.

