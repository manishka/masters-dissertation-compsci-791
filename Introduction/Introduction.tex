\chapter{Introduction} \label{chapt:Introduction}
\vspace*{\fill} 
\begin{quote} 
\centering 
``We make our world significant by the courage of our questions\\ and by the depth of our answers.''\\
\hspace*{\fill}\\
\qquad\qquad\qquad\qquad\qquad\qquad\qquad\qquad--- Carl Sagan \cite{sagan_cosmos_1981}
\end{quote}
\vspace*{\fill}
In his best-selling book \textit{Cosmos}, \citea{sagan_cosmos_1981} expresses that if we wish to be worthy of attention, then we need to ask brave questions and find their deep and meaningful answers. We think, it is our ability to observe the world around us that deepens our knowledge and broadens our horizon. This curiosity has been fundamental to the development of the technologies that we cherish everyday. With the advent of every new technology we are confronted with new possibilities and challenges. 

Today, we live in a world that is surrounded by plethora of Artificial Intelligence (AI) from virtual assistants, email spam filters, music and movie recommendation services to fraud detection systems, these AI systems have become indispensable part of our everyday life. Even though AI has been around for long time. Only recently, we have started building AI systems that learn almost autonomously, without much human intervention or expertise, for instance, self-driving cars. However, human-level intelligence or learning is very much a distant dream.

\section{Motivation}

In human life, the idea that certain things ``cause" other things, plays a vital role. In fact, we have designed our weather forecasting systems, financial markets and even statistical methods, around this philosophy. At a fundamental level, however, the particles and forces of modern physics, behave in predictable ways according to the laws of nature. It is kind of like, how a number after 55 is 56, and the number before it is 54, but 55 does not cause 54 or 56 --- there is just a ``pattern'' traced out by those numbers. Modern scientific research points out, that at the very core of this universe, there are \textit{patterns between the events}.

With this understanding about the nature of the world, we wish to manifest an artificial intelligence that can discover these patterns in real-world and explain them in a human language. Automatic Statistician is one such project that aims to create an AI that can help us to make sense of our data. \citea{lloyd_automatic_2014} created Automatic Bayesian Covariance Discovery (ABCD)  as part of this project. ABCD is artificial intelligence that discovers the patterns, which describe the given set of observations or data  and produces a report in English, like a human statistician would write. 

ABCD learns a Gaussian process model that can make precise predictions. The learnt model is interpretable due to its covariance kernel \cite{duvenaud_structure_2013}. This simple and accurate interpretation of the model, is what makes this system more useful than other systems like artificial neural networks \cite{neal_bayesian_1996}. The ability to interpret the model allows users to make sense of vast amounts of data, without possessing expert knowledge about statistical or machine learning methods.

\textit{Model Search} and \textit{Model Evaluation} are the two key phases in automatically learning a Gaussian process model. ABCD uses a greedy approach to model search \cite{lloyd_automatic_2014}. In greedy search strategy, at each stage a highest scoring kernel function is expanded for further exploration \cite{duvenaud_structure_2013}. \citea{lloyd_representation_2015} argues that greedy approach is justified as this is how a human statistician develop their models, iteratively. We think since machines can explore a much larger model space efficiently compared to human statisticians, then why should we restrict the AI search only to a limited part of model space?

Furthermore, at each stage of model search, several models are evaluated in parallel. The standard approach to Gaussian process regression is computationally expensive \cite{rasmussen_gaussian_2006}. For that reason, ABCD uses an approximation to Gaussian process regression. This evaluation technique learns kernel parameters on a randomly sampled subset of data \cite{lloyd_representation_2015}. \citea{chalupka_framework_2013} concluded that subset of data method outperforms other approximation approaches, however, the choice of subset greatly influences the prediction accuracy of the model .

Owing to the limitations discussed above and amount of experimentation carried out by \citea{lloyd_representation_2015}, we believe that further research is required in the areas of model search and evaluation of ABCD system. Our research proposal is to make model predictions stable while ensuring the high interpretability of the model.

\section{Problem statement} 
This dissertation extends the structure discovery research  \cite{duvenaud_structure_2013, lloyd_representation_2015, lloyd_automatic_2014}, to make stable and accurate extrapolation by maintaining high interpretability of the discovered structure. We are addressing this problem by proposing alternative methods to perform model search and evaluation in ABCD system. We intend to use a top-k search strategy for model space exploration and a novel cross-validated subset of data approximation to Gaussian process regression. These methods aim at improving the prediction capability of ABCD while maintaining the interpretability of its discovered models. Thus, we intend to provide stability to ABCD and refer our algorithm as Automatic Bayesian Covariance Discovery with Stable Extrapolation (ABCD-SE).
 
\section{Objectives}
Through this work, we intend to improve the prediction performance of an existing automatic data-analysis system by enabling it to search a much wider model space and by retaining the interpretability of discovered models. In other words, we aim to address the challenge of carefully choosing the methods that both explore (search a best model) and exploit our knowledge about high interpretability of kernel functions in the Gaussian processes.

In this dissertation, we have three major objectives:
\begin{itemize}
\item To develop a search strategy which expands the model search space, in turn allowing ABCD to discover richer patterns in real-world data.
\item To analyse model evaluation using Akaike information criterion (AIC) that enables ABCD to consider the amount of training data.
\item To create a model approximation technique that utilises random selection subset of data method but improves the extrapolation of discovered models.
\end{itemize}

\section{Contributions} 

To meet our objectives we carefully calibrate exploration---exploitation tradeoff. We develop a model search strategy that selects top-three models at each stage of search, this allows ABCD to search at least 60\% more model space as compared to greedy search. Using Kullback---Leibler information loss quantified by the AIC we aim to improve model selection process. Finally, we introduce a novel model evaluation method which combines the benefits of stable predicting performance provided by \textit{cross validation} with fast approximation provided by \textit{subset of data} parameter learning approach. 

Through meeting our objectives, we will make the following contributions to the field automatic machine learning:
\begin{itemize}
\item Developing an effective model search procedure that exploits the power of high performance distributed computing called \textit{top-k search}.
\item Analysing the improvement in model selection procedure by using AIC as the scoring criterion that incorporates the size of real-world data. 
\item Introducing the novel idea of combining an efficient approximation to Gaussian process regression technique known as ``Subset of Data'' (SoD) with Cross Validation (CV) to provide stable predictions, we refer this method as CV SoD.
\end{itemize}


\section{Overview of methodology} 

To meet these objectives, we have explored the background for the methods we propose, by providing in-depth insights into the areas of supervised machine learning, Bayesian non-parametric modelling and compositional kernel search. We examined multiple kernel learning methods, multimodel inference principles and several techniques that approximate to Gaussian process regression.

We have shown how our proposed methods operate and justified their applicability by analysing scientific research carried out in this field. We have conducted experiments on high performance computing platforms, additionally we describe the evaluation measures used to compare our results. 

To test our methods, we use the same, 13, univariate real-world data sets that were used by original ABCD study \cite{lloyd_representation_2015}. Moreover, we experiment with four multivariate real-numbered data sets to show the proposed methods suitability on high dimensional data. In order to assess the influence of random sampling, we  repeated all of our experiments five times.

Finally, we have shown results for testing our techniques against original approaches of greedy model search, Bayesian information criterion scoring and random selected subset of data model evaluation on all 17 datasets. We have shown that our methods achieve better and stable extrapolation while ensuring high interpretability of the models.

\section{Dissertation structure}
In this introduction chapter, we have provided some background and context for the work we have done. Our motivation comes from the realisation that AI can be a useful tool for helping us make sense of large amounts of data, where it is not always possible to have good knowledge of the statistical or machine learning methods. We have stated the contributions we intend to make through this work. We have briefly described our methodology for testing our proposed methods to show that they are suitable for the purposes outlined.

In Chapter \ref{chapt:LitRev}, we explore supervised machine learning and need for Bayesian non-parametric modelling and contrast how it is different from parametric modelling. We examine the literature by discussing automatic pattern discovery, the important considerations when performing analysis in this area, compared to traditional data analysis.  We detail how some recent compositional kernel search algorithms have been designed to meet these constraints.

In Chapter \ref{chapt:Background}, we explore the background to this work in greater depth. We detail the work and concepts that closely relate to the methods we propose in this dissertation. Specifically, we explain how Gaussian processes are used for modelling functions, how ABCD exploits this property of Gaussian processes through a greedy search strategy, how it utilises compositional covariance discovery algorithm, discuss search operators and its Bayesian Information Criterion based scoring approach to model selection and explain the underlying concept of Zero Mean Gaussian Process.

In Chapter \ref{chapt:ExpDes}, we describe the hypotheses that our dissertation is testing, we explain the steps of our proposed methods, top-k search and cross validated subset of data. We explain how they are implemented. We then describe each set of experiments undertaken, with parameters selected and clarifying the intended purposes. We clearly define the evaluation measures used for evaluating the quality and performance of our techniques. We describe the datasets we have used in our experiments, including the source of our real-world datasets. We outline the technical platform used to run our experiments.

In Chapter \ref{chapt:Results}, we tabulate the results that we have received from conducting the experiments described in the prior chapter. We provide analysis of the results for each set of experiments. We provide an overview of what we have learnt from our experiments. We relate the results seen back to the contributions, as well as any areas in which our proposed methods failed to meet our intended aims. We discuss future work, both in terms of refinement and development, of the methods we have proposed.

In Chapter~\ref{chapt:Conclusion}, we conclude the dissertation by summarising the major contributions of our work, relating them back
to our experiments, points out future directions and add some final reflections and remarks.
