\chapter{Stabilising extrapolation in an automatic data-analysis system} \label{chapt:ExpDes}

This dissertation extends the Gaussian process structure search research initiated by an automatic data-analysis system called Automatic Bayesian Covariance Discovery (ABCD). We experiment, with a much broader model search strategy like top-k, using the Akaike Information Criterion (AIC) \cite{akaike_information_1998} as model evidence instead of BIC and/ or with cross-validated Subset of Data (SoD) hyper-parameter optimisation technique for model approximation. These methods aim at improving prediction performance of ABCD, consequently we named our algorithm as Automatic Bayesian Covariance Discovery with Stable Extrapolation (ABCD-SE). 

In this section, we discuss the hypotheses that were developed as part of this dissertation. Following the hypotheses development, we present novel methods proposed in this dissertation. We then provide the design of experiments, that were performed to validate those hypotheses using proposed methods. Furthermore, we present the evaluation measure used for comparing the results of experiments. Additionally, we provide detailed information about the datasets used in the experiments. At the end of this section, we describe the software and hardware related considerations.

\section{Hypotheses development}\label{sec:hypdev}
In this section, we present the hypotheses that are tested in this dissertation and we also discuss the rationale behind the developing them. Overall, we have three hypotheses based on our focus areas i.e. model search approach, model scoring and model evaluation.

\subsection{Model search: greedy versus top-k}
In \textit{greedy search} approach, the model search begins by proposing all base kernels, these models are evaluated and a model that has a highest score is chosen as best model. In the next level of search, the best model is expanded using kernel search operators, these expanded models are again evaluated and a new best model is selected based on highest score. Section \ref{Background:Search} describes greedy approach in detail and Figure \ref{fig:searchtree} shows a typical example of greedy search. 

Greedy search approach looks very similar to step-wise forward selection approach used in linear regression \cite{hocking_analysis_1976}. Step-wise forward selection approach starts by proposing a model with no parameters. Then add one parameter at a time, if the model evidence gets better the algorithm adds new parameters in model and so on. Step-wise forward selection is often criticised in literature \cite{flom_stopping_2007} for providing poor parameter estimates. These criticisms may not be directly applicable to non-parametric Bayesian Gaussian process regression that ABCD system uses. Since the original thesis \cite{lloyd_representation_2015} did not experiment with other approaches. 

An alternative search strategy to greedy search would be to select \textit{top-k} models at each search level. In \textit{top-k search} approach the algorithm chooses top-k models that had highest scores among all proposed models and continues the tree expansion for all selected models in the next level. Analysis of reports generated by \citea{lloyd_automatic_2014}, reveal that around 80\% to 90\% of variability of data is explained by top-three components of composite kernel. We therefore, set $k=3$ and search for the top-three models at each level. Model search starts with five base kernels, top-k ($k=3$) strategy would be able to explore 60\% more model-space and possibly discover richer structures in data, as compared to greedy search. We describe covariance discovery using top-k search in detail in Section \ref{sec:algsearchtopk}.



Compared to top-k strategy, model-space explored by greedy search is very limited. Consequently, richer structures or patterns may not be discovered by greedy search at all. We formalise it in Hypothesis \ref{hyp:greedy}.

\begin{hyp}\label{hyp:greedy}
Composing a kernel greedily, results in a most interpretable Gaussian process regression model that provides stable extrapolation.
\end{hyp}

\subsection{Model score: \textrm{BIC} versus \textrm{AIC}}

Model selection is one of the biggest challenges in automatic machine learning \cite{forster_key_2000,forster_new_2001}. In literature, the problem of comparing several models, in order to best describe the given training data and assuming that, it would generalise well on unseen test data, is often referred as ``Multimodel Inference"  \cite{burnham_multimodel_2004}. 

\citea{burnham_multimodel_2004} argue that practitioners often try to improve the inference or model selection by establishing a selection criterion such as Bayesian Information Criterion (BIC) or Akaike information criterion (AIC). They describe the central issue in multimodel inference is, to first, establish the philosophy about data analysis, whether there exists a true model or that all models are just approximations? Only then, we should find a suitable criterion. They presented three guiding principles for model-based inference:
\begin{itemize}
\item \textit{\textbf{Multiple working hypotheses}} is establishing the philosophy that all proposed models are approximations to the full reality and that there is no true model.
\item \textit{\textbf{Simplicity and Parsimony}} of models is the amount of information (or truth) a particular model is representing, whether the model is extracting simple information (discovering large structures/pattern) or complex information (many local patterns). Additionally, what size of model is good enough to extract such information, does a parsimony model represent high information that is well generalisable or can large complex models be justified? This is a bias versus variance trade-off. Inference under models with too few parameters can be biased, while with models having
too many parameters may treat ``noise'' in data as signal.
\item \textit{\textbf{Strength of evidence}} is quantifying the uncertainty (or information loss) of a fitted model. This quantitative information is derivable from likelihood estimates.
\end{itemize}
 
By abiding these principles, \citea{burnham_multimodel_2004} state that a good model selection criterion should reduce to ``a number" for each fitted model. This number, quantifies the uncertainty, about each proposed model being the best model. A good criterion must also be based on the philosophy about the data are finite and ``noisy". Two well-known approaches that satisfy these requirements are Bayesian model selection based on Bayes factors and information-theoretic selection based on Kullback-Leibler (K-L) information loss. BIC represents Bayes factor approach and AIC represents the K-L information loss approach \cite{burnham_multimodel_2004}. 

ABCD system uses likelihood-based strength-of-evidence model comparison by employing BIC (Equation \ref{eq:bic}) as measure of model evidence. Model selection can also be performed using criterion as AIC with a correction $(\textrm{AIC}_c)$ for finite sample sizes (Equation \ref{eq:aicc})\cite{sugiura_further_1978}. 
\begin{equation}\label{eq:aicc}
\textrm{AIC}_c(M) = {-2\log p(D|M)} + {2p} + \frac{2p(p+1)}{(n-p-1)}
\end{equation}
where, $p$ is number of kernel parameters in trained model $M$ and $n$ total number of observations in training data $D$.


\citea{burnham_multimodel_2004} argue that BIC results in parsimonious models compared to $\textrm{AIC}$\footnote{We use $\textrm{AIC}$ and $\textrm{AIC}_c$ interchangeably in this dissertation}. It is not guaranteed, however, that this parsimony model would often represent lower information loss. This lack of certainty provides us with a research opportunity, accordingly we formulate Hypothesis \ref{hyp:bic}.

\begin{hyp}\label{hyp:bic}
Models selected with highest \textrm{BIC} score, results in a most interpretable Gaussian process regression model that provides stable extrapolation.
\end{hyp}

\subsection{Model evaluation}
The standard implementation of Gaussian Process Regression (GPR) requires $\mathcal{O}(n^2)$ space and $\mathcal{O}(n^3)$ time for a data set of $n$ examples \cite{rasmussen_gaussian_2006} and involves three phases:
\begin{itemize} 
\item \textit{\textbf{hyper-parameter learning}}: In this phase the hyper-parameters of kernel function are learned. For example, by maximizing the log marginal likelihood. Usually, this phase is computationally expensive.

\item \textit{\textbf{training}}: In this phase Gaussian process (GP) model is fitted to given training data using the learnt (or optimised) hyper-parameters. Usually this phase involves computing the Cholesky decomposition of $K + \sigma^2_n I$ on training data.

\item \textbf{\textit{testing}}: In this phase, computations are carried out on unseen test data. The computations can be significant if the test set is very large.
\end{itemize}

ABCD system carries out first two phases --- \textit{hyper-parameter learning} and \textit{training} as part of ``model evaluation''. After discovering the best covariance composition using greedy search strategy, ABCD system carries out \textit{testing} phase, the system uses the best GP model to perform predictions (or extrapolation). ABCD searches the open-ended model space by fitting large number of models, in parallel, to discover best covariance kernel composition. 

Given a large dataset or a system that needs to perform multimodel inference, GPR using standard approach would require extensive computational power. Thus, in these scenarios an approximation technique is required. \citea{chalupka_framework_2013} compared several popular approximation methods with standard implementation and concluded that Subset of Data technique outperforms all of the other approximation techniques. \textit{Subset of Data} method simply ignores some or most of the data i.e. a full GP prediction method is applied to a subset of size $m < n$. 

Accordingly, if standard approach to GPR is followed in model evaluation in a system like ABCD, it would require considerable amount of computational resources. For that reason, ABCD system uses a approximation method to perform GPR.  ABCD system uses a two-step approximation method --- first, learning of hyper-parameters on randomly sampled (or selected) subset of data and then learning hyper-parameters on full data. In this dissertation, we refer to this two-step method as Hybrid SoD method. Algorithm \ref{alg:modeleval} formalises this approach.

\begin{algorithm}[h]
\caption{Automatic Bayesian Covariance Discovery: Model Evaluation }\label{alg:modeleval}
\begin{algorithmic}[1]
\Procedure{Evaluate\_Model}{$\var{model}$, $\var{xtrain}$, $\var{ytrain}$}
      \State \multilinestate{$\var{xsub} \gets$ \Call{Random\_Sample}{$\var{xtrain}$}}
      \State \multilinestate{$\var{ysub} \gets$ \Call{Random\_Sample}{$\var{ytrain}$}}
      \\
      \For {\var{iter} in \{1 to \var{iters}\}}\
      	\State \multilinestate{$\var{theta\_opt} \gets$ \Call{Minimise}{$\var{theta\_opt}$, $\var{xsub}$, $\var{ysub}$}}
   \EndFor
        \\
      \For{\var{full\_iter} in \{1 to \var{full\_iters}\}}\
      	\State \multilinestate{$\var{theta\_opt} \gets$ \Call{Minimise}{$\var{theta\_opt}$, $\var{xtrain}$, $\var{ytrain}$}}
        \EndFor
        \\
      \State \multilinestate{$\var{likelihood} \gets$ \Call{GP\_Fit}{$\var{theta\_opt}$, $\var{xtrain}$, $\var{ytrain}$}}
      \State \textbf{return} $\var{likelihood}$
    \EndProcedure
\end{algorithmic}
\end{algorithm}

\citea{chalupka_framework_2013} stated that subset selection in SoD method could influence the model performance. ABCD system randomly chooses the subset of size $m < n$, since original thesis \cite{lloyd_representation_2015} performed all experiments only once, we do not have evidence that this random selection of subset gives stable performance or not. However, by analysing Hybrid SoD method we believe that extrapolation results are highly dependent on the seed chosen for the random number generator used for sampling of subset. 

To address this limitation, we developed a novel approximation method, that makes an informed choice when selecting a subset of data. The subset selection decision is based on 10-fold cross-validated mean absolute predicted error. We call this method as Cross-Validated SoD (CV SoD), it is described in detail in Section \ref{sec:algcvSoD}.

Based on these arguments we form Hypothesis \ref{hyp:fit}

\begin{hyp}\label{hyp:fit}
Models evaluated with Hybrid SoD method, does not always result in a most interpretable Gaussian process regression model that provides stable extrapolation.
\end{hyp}

\section{Proposed methods}\label{sec:methods}
In this section, we introduce the methods that we developed to test the alternative hypotheses. We would first describe the model search with top-k strategy while contrasting it with greedy search approach. We then move on to Cross Validated Subset of Data (CV SoD) a novel model approximation technique to Gaussian process regression.

\subsection{Kernel search with top-k strategy}\label{sec:algsearchtopk}

We, now, discuss how covariance function or composite kernel is discovered using top-three search method. Like greedy approach, top-three method starts by proposing models with only base kernels. These proposed models are evaluated, scored and ranked. Top-three ranked models are selected for next level of search. Figure \ref{fig:topksearch} shows an example of top-three models selected at first level. A proposed model containing only periodic ($\kPer$) base kernel function has scored highest and is selected as first rank model (shown in green colour), model containing only linear ($\kLin$) base kernel function is at second place (shown in red colour) and model containing only constant ($\kC$) base kernel function is ranked third (shown in blue colour). 


\begin{algorithm}[h]
\caption{ABCD: Kernel Search using top-k strategy}\label{alg:searchtopk}
\begin{algorithmic}[1]
\Procedure{Perform\_Kernel\_Search}{$\var{kernels}$, $\var{operators}$, $\var{criterion}$}
\State \multilinestate{$\var{\{M\}} \gets$ \Call{Create\_GP\_Models}{\var{kernels}}}
\State \multilinestate{$\var{\{M\}} \gets$ \Call{Random\_Restart\_Covariance\_Functions}{$\var{M}$}}\label{marker}
\State \multilinestate{$\var{\{M\}} \gets$ \Call{Remove\_Duplicate\_Models}{$\var{M}$}}
\For{\texttt{\var{model} in \var{\{M\}}}}
	\State \multilinestate{$\var{likelihoods[model]} \gets$ \Call{Evaluate\_Model}{$\var{model}$, $\var{xtrain}$, $\var{ytrain}$}}
	\State \multilinestate{$\var{scores[model]} \gets$ \Call{Calculate\_Score}{$\var{likelihoods[model]}$, $\var{criterion}$}}
\EndFor
\State \multilinestate{$\var{top\_k\_models} \gets$ \Call{Get\_Top\_K\_Models}{$\var{scores}$, $\var{criterion}$}}
\If {$\var{depth} \leq \var{desired\_depth}$ \quad \textbf{and} \newline \qquad \Call{Is\_Model\_Score\_Improved}{$\var{scores[top\_k\_models]}$, $\var{threshold}$}}
    \State $\var{depth} \gets \var{depth} + 1$
    \State \multilinestate{$\var{\{M\}} \gets$ \Call{Expand\_Kernels}{\var{M[top\_k\_models]}, \var{kernels}, \var{operators}}}
    \State \Goto{marker}
\EndIf
	\State \textbf{return} $\var{M[top\_k\_models]}$
\EndProcedure
\end {algorithmic}
\end{algorithm}

	
    
In next search level, each of these selected models are expanded by applying all search operators described in Equations \ref{eq:add}, \ref{eq:prod} and \ref{eq:replace}. This expansion results in a new set of proposed models. These proposed models are evaluated, scored and ranked. Among those scored models, top-three are selected for next level of search. Figure \ref{fig:topksearch2} shows the top-three models selected at second level, they are represented as first ranked model in green colour, second ranked model in red colour and third ranked model in blue colour. In Figure \ref{fig:topksearch2}, models proposed after expansion of constant ($\kC$) base kernel would not longer be part of further search, as none of its proposed model made into top-three ranks. However, search would be continued in two sub-branches of linear ($\kLin$) base kernel.



\begin{figure}[h]
\centering
\begin{tikzpicture}
\node [shape=rectangle,draw,thick] (v1) at (0,4) {Start}; 
\node (v2) at (5,2) {\kWN};
\node [shape=rectangle,draw,thick,fill=blue!15] (v3) at (2.5,2) {\kC};
\node [shape=rectangle,draw,thick,fill=red!15] (v4) at (0,2) {\kLin};
\node [shape=rectangle,draw,thick,fill=green!15] (v5) at (-2.5,2) {\kPer};
\node (v6) at (-5,2) {\kSE};
\draw  (v1) edge (v2);
\draw  (v1) edge (v3);
\draw  (v1) edge (v4);
\draw  (v1) edge (v5);
\draw  (v1) edge (v6);
\end{tikzpicture}
\caption{Covariance function discovery using top-three search approach at depth level 1}
\label{fig:topksearch}
\end{figure}


\begin{figure}[h]
\centering
\begin{tikzpicture}
\node[shape=rectangle,draw,thick] (v1) at (1.5,3) {Start};
\node (v2) at (-4,1) {\kSE};
\node [shape=rectangle,draw,thick,fill=green!15](v3) at (-1.5,1) {\kPer};
\node [shape=rectangle,draw,thick,fill=red!15](v4) at (2,1) {\kLin};
\node [shape=rectangle,draw,thick,fill=blue!15](v5) at (5,1) {\kC};
\node (v6) at (7.5,1) {\kWN};
\draw  (v1) edge (v2);
\draw  (v1) edge (v3);
\draw  (v1) edge (v4);
\draw  (v1) edge (v5);
\draw  (v1) edge (v6);

\node  [shape=rectangle,draw,thick,fill=green!15] (v1) at (-7,-2.5) {$\kPer + \kSE$};
\node  (v7) at (-5,-2.5) {\ldots};
\node  (v8) at (-3,-2.5) {$\kPer \times \kWN$};
\node  [shape=rectangle,draw,thick,fill=red!15](v9) at (0,-2.5) {$\kLin + \kSE$};
\node  (v10) at (1.5,-2.5) {\ldots};
\node  [shape=rectangle,draw,thick,fill=blue!15](v11) at (3.5,-2.5) {$\kLin \times \kLin$};
\node  (v12) at (5,-2.5) {\ldots};
\node  (v13) at (7,-2.5) {$\kC + \kSE$};
\node  (v14) at (8.5,-2.5) {\dots};
\draw  (v3) edge (v1);
\draw  (v3) edge (v7);
\draw  (v8) edge (v3);
\draw  (v4) edge (v9);
\draw  (v4) edge (v10);
\draw  (v4) edge (v11);
\draw  (v4) edge (v12);
\draw  (v5) edge (v13);
\draw  (v5) edge (v14);
\end{tikzpicture}
\caption{Covariance function discovery using top-three search approach at depth level 2}
\label{fig:topksearch2}
\end{figure}

\begin{figure}[h]
\centering
\begin{tikzpicture}
\node[shape=rectangle,draw,thick] (v1) at (1.5,3) {Start};
\node (v2) at (-4,1) {$\kSE$};
\node [shape=rectangle,draw,thick,fill=green!15](v3) at (-1.5,1) {$\kPer$};
\node [shape=rectangle,draw,thick,fill=red!15](v4) at (2,1) {$\kLin$};
\node [shape=rectangle,draw,thick,fill=blue!15](v5) at (5,1) {$\kC$};
\node (v6) at (7.5,1) {$\kWN$};
\draw  (v1) edge (v2);
\draw  (v1) edge (v3);
\draw  (v1) edge (v4);
\draw  (v1) edge (v5);
\draw  (v1) edge (v6);

\node  [shape=rectangle,draw,thick,fill=green!15](v1) at (-7,-1) {$\kPer + \kSE$};
\node  (v7) at (-5,-1) {\ldots};

\node  [shape=rectangle,draw,thick,fill=red!15] (v9) at (-1,-1) {$\kLin + \kSE$};
\node  (v10) at (3,-1) {\ldots};
\node  [shape=rectangle,draw,thick,fill=blue!15] (v11) at (6.5,-1) {$\kLin \times \kLin$};
\node  (v14) at (9,-1) {\dots};
\draw  (v3) edge (v1);
\draw  (v3) edge (v7);
\draw  (v4) edge (v9);
\draw  (v4) edge (v10);
\draw  (v4) edge (v11);
\draw  (v5) edge (v14);

\node (v15) at (-7,-4) {\ldots};
\node  [shape=rectangle,draw,thick,fill=green!15](v19) at (-3.5,-4) {$(\kLin + \kSE) + \kSE$};
\node (v16) at (-1.2,-4) {\ldots};
\node  [shape=rectangle,draw,thick,fill=red!15](v17) at (1,-4) {$(\kLin + \kSE) + \kLin$};
\node (v21) at (3.8,-4) {\ldots};
\node  [shape=rectangle,draw,thick,fill=blue!15](v18) at (6.5,-4) {$(\kLin \times \kLin) + \kSE$};
\node (v20) at (9,-4) {\ldots};
\draw  (v1) edge (v15);
\draw  (v9) edge (v16);
\draw  (v9) edge (v17);
\draw  (v11) edge (v18);
\draw  (v19) edge (v9);
\draw  (v20) edge (v11);
\draw  (v9) edge (v21);
\end{tikzpicture}
\caption{Covariance function discovery using top-three search approach at depth level 3}
\label{fig:topksearch3}
\end{figure}


In search level three, we further expand the selected models by applying search operators. Then evaluate, score and rank them based on the scores. Figure \ref{fig:topksearch3} shows how search might proceed in level three. In this example we can see that top-three search has discovered a structure, which would not have been possible if greedy search was used. In second level, Model with kernel function as ($\kLin + \kSE$) had a lower score than that of a model with kernel function as ($\kPer + \kSE$). Whereas, in level three, all proposed models obtained after the expansion of ($\kPer + \kSE$), scored less than those models that were generated by expanding ($\kLin + \kSE$). 

\subsection{Model evaluation with CV SoD method}\label{sec:algcvSoD}

In CV SoD method, full training data is divided ten equal parts (or folds), we then perform 10 iterations over it. In each iteration, nine folds are chosen as train-set and a remaining fold is chosen as validate-set. Then, in next iteration other nine folds are chosen as train-set and a fold that was part of train-set earlier is now chosen as validate-set. In this way, each fold is once chosen as validate-set. 

Inside each iteration, a random sample is chosen from train-set called as subset of data (SoD). We then optimise hyper-parameters on SoD, fit a GP model on SoD and perform prediction on validate-set. After 10 iterations we get 10 predictions and the GP model which results in minimum mean absolute prediction error, its hyper-parameter are further optimised on full training data, a final GP model is fitted on full training data and likelihood is returned. We formalise this procedure in Algorithm \ref{alg:evalcvsod}.

\begin{algorithm}[h]
\caption{ABCD: Model Evaluation using CV SoD method}\label{alg:evalcvsod}
\begin{algorithmic}[1]
\Procedure{Evaluate\_Model}{$\var{model}$, $\var{xtrain}$, $\var{ytrain}$}
	\State \multilinestate{$\var{k} \gets$ 10}
	\State \multilinestate{$\var{xtrain\_cv} \gets$ \Call{Make\_CV\_Partition}{$\var{xtrain}$, $\var{k}$}}
    \State \multilinestate{$\var{ytrain\_cv} \gets$ \Call{Make\_CV\_Partition}{$\var{ytrain}$, $\var{k}$}}
    \\
	\For{\var{fold} in \{1 to k\} }
      \State \multilinestate{$\var{xsub} \gets$ \Call{Random\_Sample}{$\var{xtrain\_cv[!fold]}$}}
      \State \multilinestate{$\var{ysub} \gets$ \Call{Random\_Sample}{$\var{ytrain\_cv[!fold]}$}}
      \State \multilinestate{$\var{xvalid} \gets$ \var{xtrain\_cv[fold]}}
      \State \multilinestate{$\var{yvalid} \gets$ \var{ytrain\_cv[fold]}}
      \\
      \For {\var{iter} in \{1 to \var{iters}\}}\
      	\State \multilinestate{$\var{theta\_opt[fold]} \gets$ \Call{Minimise}{$\var{theta\_opt[fold]}$, $\var{xsub}$, $\var{ysub}$}}
   \EndFor
   \\
         \State \multilinestate{$\var{ypred} \gets$ \Call{GP\_Fit}{$\var{theta\_opt[fold]}$, $\var{xsub}$, $\var{ysub}$, $\var{xvalid}$}} 
         \State \multilinestate{$\var{MAE[fold]} \gets$ \Call{Mean\_Of\_Absolute\_Differences}{$\var{ypred}$ - $\var{yvalid}$}}
    \EndFor
    \\
    \State \multilinestate{$\var{theta\_opt} \gets$ \var{theta\_opt[\Call{Index\_Of\_Min}{$\var{MAE}$}]}}
    \\
      \For{\var{full\_iter} in \{1 to \var{full\_iters}\}}\
      	\State \multilinestate{$\var{theta\_opt} \gets$ \Call{Minimise}{$\var{theta\_opt}$, $\var{xtrain}$, $\var{ytrain}$}}
        \EndFor
        \\
      \State \multilinestate{$\var{likelihood} \gets$ \Call{GP\_Fit}{$\var{theta\_opt}$, $\var{xtrain}$, $\var{ytrain}$}}
      \State \textbf{return} $\var{likelihood}$
    \EndProcedure
\end{algorithmic}
\end{algorithm}


\section{Design of experiments}\label{sec:expdesc}
This dissertation extends the research carried out by \citea{lloyd_automatic_2014} by performing experiments in the areas of model search, model scoring and model evaluation. We divide our study into three parts, they are baseline experiment (Section \ref{sec:baselineExp}), assessments with Hybrid SoD method (Section \ref{sec:hybSoD}) and assessments with CV SoD method (Section \ref{sec:cvSoD}). In each set of experiments we opt for a particular model searching method and a model scoring criteria, this is based on a $2 \times 2 \times 2$ factorial or balanced design. Table \ref{tab:experiments} gives a list of experiments performed in this dissertation.

In each of our experiments the given dataset was divided into two parts training data (90\%) and test data (10\%). We discovered the best model on training data and made predictions on test data. We used five base kernels as shown in Figure \ref{fig:atomskl} and three search operators as described in Equations \ref{eq:add}, \ref{eq:prod} and \ref{eq:replace}. Specific details about experimental set-up are mentioned in their respective subsections.

\begin{table}[h]
\centering
\caption{List of experiments}\label{tab:experiments}
\bgroup
\def\arraystretch{1.5}
\begin{tabular}{llcl}
\hline
\multicolumn{1}{c}{\bfseries{Experiment}} & \multicolumn{1}{c}{\bfseries{Model Search}} & \multicolumn{1}{c}{\bfseries{Model Score}} \\ \hline
\textbf{Baseline} & Greedy & BIC \\ \hline
\multirow{4}{*}{\textbf{With Hybrid SoD method}} & Greedy & BIC  \\
 & Greedy & AIC  \\
 & Top-k  & BIC \\
 & Top-k  & AIC  \\ \hline
\multirow{4}{*}{\textbf{With CV - SoD method}} & Greedy & BIC  \\
 & Greedy & AIC  \\
 & Top-k  & BIC  \\
 & Top-k  & AIC  \\ \hline
\end{tabular}
\egroup

\end{table}

\subsection{Baseline Experiments}\label{sec:baselineExp}
The purpose of these experiments were to make sure that using GNU Octave instead of Matlab \footnote{Matlab was used by \citea{lloyd_automatic_2014} for their experiments} produces similar results. In baseline experiments, we performed model search and prediction without any change in ABCD system i.e. we used the Algorithm \ref{alg:kernsearch} approach to discover covariance kernel function greedily. 

We did not experiment with any of the system parameters with different values, as the purpose was to establish a baseline, we kept same values as set by original thesis \cite{lloyd_representation_2015}, for example, we used the same random seeds, kept $\var{iters} = 250$, $\var{full\_iters} = 10$, $\var{subset\_size} = 250$ and $\var{desired\_depth} = 10$. We performed these experiments on four out of 13 univariate datasets used in original thesis \cite{lloyd_representation_2015}, they were 01-airline, 06-internet,  07-call-centre and 08-radio.

\subsection{Experiments with Hybrid SoD method}\label{sec:hybSoD}
The purpose of these experiments was to assess the influence of randomly selection subset of data in Hybrid SoD model evaluation method. We performed four experiments, as seen in Table \ref{tab:experiments}. In first experiment we used greedy search strategy with \textrm{BIC} as model selection criterion. In second, we used greedy search with \textrm{AIC} as model selection criterion. In third and fourth, we used top-k search approach with \textrm{BIC} and \textrm{AIC} as model selection criterion, respectively. These experiments were performed to validate all the hypotheses mentioned Section \ref{sec:hypdev}. 

We performed each experiment five times on 13 univariate datasets and four multi-variate datasets, each time we used a different random seed. As the purpose of these experiments was to assess the influence of model evaluation on extrapolation and due to the shared allocation of hardware infrastructure, we restricted model search to three levels deep (i.e. $\var{desired\_depth} = 3$). Moreover, we did not experiment with system parameters and kept them as what they were used in original thesis \cite{lloyd_representation_2015}, for example, we kept $\var{iters} = 250$, $\var{full\_iters} = 10$ and $\var{subset\_size} = 250$.

\subsection{Experiments with CV SoD method}\label{sec:cvSoD}
The purpose of these experiments was to assess the influence of cross-validated selection subset of data using novel CV SoD model evaluation method. We performed four experiments, as seen in Table \ref{tab:experiments}. In first experiment we used greedy search strategy with \textrm{BIC} as model selection criterion. In second, we used greedy search with \textrm{AIC} as model selection criterion. In third and fourth, we used top-k search approach with \textrm{BIC} and \textrm{AIC} as model selection criterion, respectively. These experiments were performed to validate all the hypotheses mentioned Section \ref{sec:hypdev}. 

We performed each experiment five times on 13 univariate datasets and four multi-variate datasets, each time we used a different random seed. As the purpose of these experiments was to assess the influence of model evaluation on extrapolation and due to the shared allocation of hardware infrastructure, we restricted model search to three levels deep (i.e. $\var{desired\_depth} = 3$). Moreover, we did not experiment with system parameters and kept them as what they were used in original thesis \cite{lloyd_representation_2015}, for example, we kept $\var{iters} = 250$, $\var{full\_iters} = 10$ and $\var{subset\_size} = 250$.

\section{Error measures}
The best Gaussian process models, that were constructed by performing the experiments described in Sections \ref{sec:hybSoD} and \ref{sec:cvSoD}, were used for predicting the test data (10\% of original dataset). We measured the accuracy of each model's prediction on test data using the Mean Squared Error (MSE) (Equation \ref{eq:mse}) and then standardised it, so as to compare the model performance of different methods on different datasets. \citea{rasmussen_gaussian_2006} defined Standardised Mean Squared Error (SMSE) in their book and we formulated it in Equation \ref{eq:smse}.
\begin{equation}\label{eq:mse}
\textrm{MSE}(M) = \mu{\left([\outputVar_{test} - \outputVar_{pred}]^2\right)}
\end{equation}
\hspace*{\fill}
\begin{equation}\label{eq:smse}
\textrm{SMSE}(M) = \frac{\textrm{MSE}(M)}{\textrm{Var}(\outputVar_{test}) + \left(\mu(\outputVar_{test})\right)^2}
\end{equation}

where, $\outputVar_{test}$ is the vector of output target values of test data, $\outputVar_{pred}$ is a vector of predicted output values, predicted by model $M$, $\mu$ is statistical mean, $\textrm{Var}$ is the sample variance.

\section{Datasets used}

We used 17 real world datasets. We performed experiments on 13 datasets that were used originally by \citea{lloyd_automatic_2014} (refer datasets 1 to 13 in Table \ref{tab:dataset}), these datasets are available in their repository. The remaining four datasets were borrowed from various sources, their description is given below.

\subsubsection{14-quebec-xl: Quebec births dataset}
\textbf{Abstract}: Number of daily birth in Quebec, from Jan 1, 1977 to Dec 31, 1990. Available at \url{http://research.ics.aalto.fi/eiml/datasets.shtml}

\subsubsection{15-concrete: Concrete Compressive Strength Data Set}
\textbf{Abstract}: Concrete is the most important material in civil engineering. The concrete compressive strength is a highly nonlinear function of age and ingredients. Available at  \url{https://archive.ics.uci.edu/ml/datasets/Concrete+Compressive+Strength}. 

\subsubsection{16-parkinsons: Parkinsons Telemonitoring Data Set}
\textbf{Abstract}: Oxford Parkinson's Disease Telemonitoring Dataset. Available at \url{https://archive.ics.uci.edu/ml/datasets/Parkinsons+Telemonitoring}.

\subsubsection{17-winewhite: Wine Quality Data Set}
\textbf{Abstract}: Dataset includes white vinho verde wine samples, from the north of Portugal. The goal is to model wine quality based on physicochemical tests. Available at \url{https://archive.ics.uci.edu/ml/datasets/Wine+Quality}

\begin{table}[h]
\centering
\caption{Dataset description}\label{tab:dataset}
\bgroup
\def\arraystretch{1.5}
\begin{tabular}{lcccl}
\hline
\multicolumn{1}{c}{\multirow{2}{*}{\textbf{Dataset}}} & \multicolumn{1}{c}{\textbf{Train Set}} &  \multicolumn{1}{c}{\textbf{Test Set}}	&	\multicolumn{2}{l}{\textbf{Characteristics}}\\ 
\multicolumn{1}{c}{} & \#instances  & \#instances & dataset & attribute \\ \hline
01-airline & 129 & 15 & Real & Univariate \\
02-solar & 361 & 41 & Real & Univariate  \\
03-mauna & 490  & 55 & Real & Univariate \\
04-wheat & 333  & 37 & Real & Univariate \\
05-min-temp & 899  & 101 & Real & Univariate \\
06-internet & 909  & 91 & Real & Univariate\\
07-calls & 162  & 18 & Real & Univariate\\
08-radio & 216  & 24 & Real & Univariate  \\
09-gas & 428  & 48 & Real & Univariate\\
10-sulphuric & 415 & 47 & Real & Univariate \\
11-unemploy & 367 & 41 & Real & Univariate \\
12-quebec & 893 & 107 & Real & Univariate \\
13-wages & 661 & 74 & Real & Univariate\\
14-quebec-xl & 4602 & 511 & Real & Multivariate (= 5)\\
15-concrete & 927 & 103 & Real & Multivariate (= 9)\\
16-parkinsons & 5287 & 588 & Real & Multivariate (= 21)\\
17-winewhite & 4408 & 490 & Real & Multivariate (= 12)
\end{tabular}
\egroup

\end{table}

\section{Technical considerations}
In this section, we describe the existing software tools and libraries used to stabilise extrapolation in an automatic data-analysis system and hardware platforms on which the experiments were run.

\subsection{Software considerations}\label{sec:soft}
We used Automatic Bayesian Covariance Discovery (ABCD) system as our automatic data-analysis system. The implementation of ABCD, we used was originally created as part of the automatic statistician ({\url{http://www.automaticstatistician.com}) project and is available at \url{https://github.com/jamesrobertlloyd/gpss-research}. 

This implementation of ABCD system uses a modified version Gaussian process regression and classification toolbox version 3.1, this toolbox is popularly known as Gaussian Processes for Machine Learning (GPML) toolbox, available at \url{http://www.gaussianprocess.org/gpml/code}. 

We ran ABCD system using Python version 2.7.9 with numpy (1.9.2) and scipy (0.15.1) libraries. We used GNU Octave version 4.0.0 to interface with GPML toolbox. We performed our experiments on machines running Red Hat Enterprise Server release 6.3 (Santiago) with Linux version 2.6.32-279.14.1.el6.x86\_64 (mockbuild@x86-002.build.bos.redhat.com) as their operating systems. 


\subsection{Hardware considerations}
All experiments were performed using High Performance Computing (HPC) facilities provided by New Zealand eScience  Infrastructure (NeSI). We performed Gaussian process model search on the nodes that supported Advanced Vector Extensions (AVX) instruction set i.e. NeSI nodes with Sandy Bridge or Ivy Bridge architecture \footnote{\url{http://www.eresearch.auckland.ac.nz/en/centre-for-eresearch/research-services/computing-resources.html}}. Table \ref{tab:avxnodes} shows the AVX node specifications. 
\begin{table}[h]
\centering
\caption{NeSI's AVX Node specifications}\label{tab:avxnodes}
\bgroup
\def\arraystretch{1.5}
\begin{tabular}{lll}
\hline
\textbf{Architecture}                                                     & \textbf{SandyBridge} & \textbf{IvyBridge} \\ \hline
\textbf{CPU Model}                                                        & Intel E5-2680        & Intel E5-2697      \\
\textbf{Clock (GHz)}                                                      & 2.7                  & 2.7                \\
\textbf{Cache (MB)}                                                       & 20                   & 30                 \\
\textbf{CPU Cores per node}   & 16                   & 24                 \\
\textbf{Memory (GB) per node} & 128                  & 128 or 256         \\
\end{tabular}
\egroup

\end{table}

\subsection{Source code availability}
The code changes that were performed as part of this dissertation at made available at \url{https://bitbucket.org/manishka/abcd-research2}. This repository is trimmed down version of original repository mentioned in Section \ref{sec:soft}. Our repository contains code changes to support NeSI platform, GNU Octave scripting and also implements model scoring and model evaluation techniques discussed in Section \ref{sec:methods}.

\section{Summary}
Through this chapter, we described the challenges and criticism that are faced by ABCD --- an existing automatic data-analysis system. We developed our hypotheses to tackle those problems. We described ABCD-SE's proposed methods (with their algorithms) and set of experiments designed to test those hypotheses. Furthermore, we discussed the error measure, called Standardised Mean Squared Error (SMSE). This will be used to compare our proposed algorithms to existing system. We mentioned the range of real-world
datasets, used to compare the various approaches. Finally, we described the technical platforms used to evaluate our proposed methods.
In the next chapter, we examine the results of these experiments.