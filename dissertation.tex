\documentclass[12pt,examcopy]{uathesis}
\usepackage{url}
\usepackage{array}
\usepackage{amsmath,amssymb,amsfonts,textcomp}
\usepackage{booktabs}
\usepackage{relsize}
\usepackage{nicefrac}
\usepackage{nth}
\usepackage{acronym}
\usepackage{bm}
\usepackage{graphicx}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{moreverb}
\usepackage{xcolor,listings}
\usepackage{microtype}
\lstset{upquote=true}
\usepackage[colorlinks=true,urlcolor=blue,citecolor=blue,linkcolor=blue,bookmarks=true]{hyperref}
\usepackage[defernumbers=true,backend=bibtex]{biblatex}
\addbibresource{Zotero.bib}
\usepackage{tikz}
\usetikzlibrary{calc}
\usetikzlibrary{shapes,arrows}
\newtheorem{definition}{Definition}
\usepackage{tabularx}
\usepackage{varwidth}
\usepackage{multirow}
\usepackage{ntheorem}
\usepackage{longtable}
\usepackage[justification=centering]{caption}
%\usepackage{mathptmx}
\usepackage[scaled=.90]{helvet}
\usepackage{pdfpages}
\usepackage[toc,page]{appendix}
\usepackage{wrapfig}
\usepackage{lscape}
\usepackage[counterclockwise, figuresleft]{rotating}
\usepackage{comment}
\usepackage{color}
\usepackage[scientific-notation=true]{siunitx}
\includecomment{comment}
\DeclareCaptionFont{xipt}{\fontsize{11}{13}\mdseries}
\usepackage[font=xipt,labelfont=bf]{caption}
%% for hyp and subhyp
\newtheorem{hyp}{Hypothesis}

\makeatletter
\newcounter{subhyp} 
\let\savedc@hyp\c@hyp
\newenvironment{subhyp}
 {%
  \setcounter{subhyp}{0}%
  \stepcounter{hyp}%
  \edef\saved@hyp{\thehyp}% Save the current value of hyp
  \let\c@hyp\c@subhyp     % Now hyp is subhyp
  \renewcommand{\thehyp}{\saved@hyp\alph{hyp}}%
 }
 {}
\newcommand{\normhyp}{%
  \let\c@hyp\savedc@hyp % revert to the old one
  \renewcommand\thehyp{\arabic{hyp}}%
} 
\makeatother

%% new commands and defs
\newcommand{\acro}[1]{\textsc{#1}}
\def\ie{i.e.\ }
\def\eg{e.g.\ }
\def\iid{i.i.d.\ }
\def\simiid{\sim_{\mbox{\tiny iid}}}
\def\eqdist{\stackrel{\mbox{\tiny d}}{=}}

\def\Reals{\mathbb{R}}

\def\Uniform{\mbox{\rm Uniform}}
\def\Bernoulli{\mbox{\rm Bernoulli}}
\def\GP{\mathcal{GP}}

\def\inputVar{x}
\def\InputVar{X}
\def\InputSpace{\mathcal{X}}
\def\outputVar{y}
\def\OutputSpace{\mathcal{Y}}
\def\function{f}
\def\kernel{k}
\def\KernelMatrix{K}
\def\SumKernel{\sum}
\def\ProductKernel{\prod}
\def\expression{e}

\def\SE{\acro{SE}}
\def\Per{\acro{Per}}
\def\RQ{\acro{RQ}}
\def\Lin{\acro{Lin}}

\def\subexpr{\mathcal{S}}
\def\baseker{\mathcal{B}}
\def\numWinners{k}

\newcommand{\kSE}{{\acro{SE}}}
\newcommand{\kC}{{\acro{C}}}
\newcommand{\kPer}{{\acro{Per}}}
\newcommand{\kLin}{{\acro{Lin}}}
\newcommand{\kWN}{{\acro{WN}}}
\newcommand{\kCP}{{\acro{CP}}}
\newcommand{\kCW}{{\acro{CW}}}
\newcommand{\kRQ}{{\acro{RQ}}}
\newcommand{\gp}{{\acro{gp}}}
\newcommand{\bmc}{{\acro{bmc}}}
\newcommand{\bq}{{\acro{bq}}}
\newcommand{\sbq}{{\acro{sbq}}}
\mathchardef\mhyphen="2D
%% commands for atoms figs
\newcolumntype{x}[1]{>{\centering\arraybackslash\hspace{0pt}}m{#1}}
\newcommand{\fhbig}{1.5cm}
\newcommand{\fwbig}{1.5cm}
\newcommand{\kernpic}[1]{\includegraphics[height=\fhbig,width=\fwbig]{figures/structure_examples/#1}}
\newcommand{\colsize}{2.7cm}
\newcommand{\sepsize}{0.0cm}

% commands for algorithm
\newcommand{\var}{\texttt}
\newcommand{\assign}{\leftarrow}
\newcommand{\multilinestate}[1]{%
  \parbox[t]{\linewidth}{\raggedright\hangindent=\algorithmicindent\hangafter=1
    \strut#1\strut}}
\algnewcommand{\algorithmicgoto}{\textbf{go to}}%
\algnewcommand{\Goto}[1]{\algorithmicgoto~\ref{#1}}%

% some settings to fill pages more densely
\renewcommand\floatpagefraction{.9}
\renewcommand\topfraction{.9}
\renewcommand\bottomfraction{.9}
\renewcommand\textfraction{.1}
\setcounter{totalnumber}{50}
\setcounter{topnumber}{50}
\setcounter{bottomnumber}{50}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%     DOCUMENT BEGIN     %%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%          			TITLE PAGE      					
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\titleimage{figures/uoa}
\title{ABCD-SE: Automatic Bayesian Covariance Discovery with Stable Extrapolation}
\author{Manish Ashoklal Kukreja}
\department{Computer Science}
\supervisor{Dr. Yun Sing Koh}{Dr. Patricia Jean Riddle}
\maketitle


\frontmatter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%          			ABSTRACT      					
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}
Machine learning algorithms are known for their predictive power --- feed input to algorithm and you get a output with its likelihood or probability. Ensemble methods like random forest, artificial neural networks such as deep learning or statistical models, for example, support vector machines, often provide high accuracy. These algorithms first go through a process called ``learning or modelling'' the data. In learning phase, algorithms discover certain set of rules or parameters, usually referred as \textit{models}. To most users, these learned models are seen as \textit{inscrutable}, since many of them are not given training in machine learning or statistics.

It is a big challenge, in artificial intelligence (AI) field, to learn (or select) a model that not only predicts the data well, but also is interpretable. As we try to make our models more descriptive, the chances of over-fitting increases. Bayesian non-parametric modelling, specifically Gaussian processes (GP) provide a way to fit a model that can be descriptive, interpretable and has more predictive power. GP can be learned easily on new data or model change-points in data via their covariance kernel function. 

This dissertation explore several key areas of an artificial intelligent system which performs data analysis through compositional kernel search called Automatic Bayesian Covariance Discovery (ABCD). We introduce a novel methods to stabilise the predictive performance of ABCD. From our experimental results, we show that our methods perform better extrapolation and can also explore a much wider model space in hopes of searching a richer interpretable structure. Thus, this dissertation is titled as Automatic Bayesian Covariance Discovery with Stable Extrapolation or ABCD-SE.

\end{abstract}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%          		ACKNOWLEDGEMENTS      					
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{acknowledgements}
I would like to acknowledge and thank all the people and organisations that helped me with my dissertation.

First and foremost, I express my gratitude and respect to both of my supervisors Yun Sing Koh and Patricia Jean Riddle, for their constant guidance, pragmatic suggestions and useful feedback throughout this project. Their open and forward looking attitude towards research and reproducibility defined the success for this project.

I would like to give a special thanks to James Robert Lloyd, who made his work of Automatic Bayesian Covariance Discovery algorithm as public, and thus I could extend his research, in the form of this dissertation.

I  wish  to  acknowledge  the  contribution  of  New  Zealand  eScience  Infrastructure (NeSI)  high-performance  computing facilities  to  the  results  of  this  research.  NZ's national  facilities  are  provided  by  the  NZ  eScience  Infrastructure  and funded  jointly  by  NeSI's  collaborator  institutions  and  through  the  Ministry of  Business,  Innovation  \&  Employment's  Research  Infrastructure programme \url{https://www.nesi.org.nz}. 

Finally, I wish to thank my family, starting with my wife, Deepa Manish Kukreja, who kept me motivated towards the completion of this study and my Masters. My two very energetic kids Dron and Paarth who always cheered me up whenever I felt stressed throughout this work. Lastly, I thanks my parents for their blessings and support that I receive over the course of this work and beyond.

\end{acknowledgements}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%          		TABLE OF CONTENTS      					
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setcounter{tocdepth}{2}
\tableofcontents
\cleardoublepage
\phantomsection
\addcontentsline{toc}{chapter}{\listtablename}
\listoftables
\cleardoublepage
\addcontentsline{toc}{chapter}{\listfigurename}
\listoffigures
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%          			CHAPTERS      					
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\mainmatter
\include{Introduction/Introduction}
\include{Literature_Review/Literature}
\include{Background/Background}
\include{Experimental_Design/Experiment}
\include{Results/Results}
\include{Conclusion/Conclusion}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%          			APPENDIX      					
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{appendices} % Using appendices environment for more functionality

%\chapter{Appendix Title} 
%\section*{Section Title}
\input{Appendix/appendix}

\end{appendices}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%          			REFERENCES      					
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\backmatter
\def\bibfont{\footnotesize}\printbibliography[nottype=misc, title={References}]
\def\bibfont{\footnotesize}\printbibliography[type=misc,heading=subbibliography, title={Online Resources}]


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%     DOCUMENT ENDS     %%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}

